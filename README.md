# Webxx
Some thoughts on a simple C++ based web server

## Supporting Technologies
Outlining a simple HTTP server based around:
 * Boost::asio
 * Boost::beast
 * Boost::fibers
 
 Asio provides an asynchronous network abstraction, while fibers return the logic to straight line linear code.
 
 The experiment is how to use this to provide a simple interface over a scalable core.
 
 ## Licensing
 The main code is licensed under the GPL v3.
 
 Other Licenses:
  * The code under the external/asio and external catch2 directory is under the Boost license.
  The license is included in the external directory.
  
 