/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <webxx/router.hh>

#include <regex>

#include <boost/algorithm/string/find_iterator.hpp>
#include <boost/algorithm/string/finder.hpp>

namespace webxx
{
    namespace detail
    {
        struct NullParamCallback
        {
            void
            operator( )( std::string value, std::string key )
            {
            }
        };

        class ParamExtractionCallback
        {
        public:
            explicit ParamExtractionCallback( path_parameter_list& param_list )
                : param_list_{ param_list }
            {
            }
            ParamExtractionCallback( const ParamExtractionCallback& other ) =
                default;
            ParamExtractionCallback( ParamExtractionCallback&& other ) =
                default;

            void
            operator( )( std::string value, std::string key )
            {
                param_list_.push_back( std::move( value ), std::move( key ) );
            }

        private:
            path_parameter_list& param_list_;
        };

        template < typename ParamCallback >
        class PathSegmentMatcher : boost::static_visitor< bool >
        {
        public:
            explicit PathSegmentMatcher( ParamCallback& cb )
                : param_callback_{ cb }
            {
            }
            PathSegmentMatcher( const PathSegmentMatcher& other ) = default;
            PathSegmentMatcher( PathSegmentMatcher&& other ) noexcept = default;
            PathSegmentMatcher&
            operator=( const PathSegmentMatcher& other ) = default;
            PathSegmentMatcher&
            operator=( PathSegmentMatcher&& other ) noexcept = default;

            bool
            operator( )( const std::string& s ) const
            {
                return *cur_ == s;
            }
            bool
            operator( )( const empty_param& e ) const
            {
                return cur_->empty( );
            }
            bool
            operator( )( const int_param& i ) const
            {
                bool first = true;
                for ( const auto& ch : *cur_ )
                {
                    if ( ( ch == '-' || ch == '+' ) && first )
                    {
                        first = false;
                        continue;
                    }
                    if ( ch < '0' || ch > '9' )
                    {
                        return false;
                    }
                    first = false;
                }
                param_callback_( *cur_, i.name );
                return true;
            }
            bool
            operator( )( const string_param& s ) const
            {
                param_callback_( *cur_, s.name );
                return true;
            }

            void
            cur_segment( const std::string* s )
            {
                cur_ = s;
            }

        private:
            ParamCallback&     param_callback_;
            const std::string* cur_{ nullptr };
        };

        template < typename ParamCallback >
        bool
        pattern_matches_impl( const segmented_path& segments,
                              const std::string&    path,
                              ParamCallback&        callback )
        {
            std::string delim( 1, '/' );
            auto        it = boost::make_split_iterator(
                path, boost::first_finder( delim, boost::is_equal( ) ) );

            PathSegmentMatcher< ParamCallback > matcher( callback );

            auto range = boost::make_iterator_range( it, decltype( it )( ) );
            auto match_it = segments.begin( );
            for ( const auto& segment : range )
            {
                if ( match_it == segments.end( ) )
                {
                    return false;
                }
                std::string cur( segment.begin( ), segment.end( ) );
                matcher.cur_segment( &cur );
                if ( !boost::apply_visitor( matcher, *match_it ) )
                {
                    return false;
                }
                ++match_it;
            }
            if ( match_it != segments.end( ) )
            {
                return false;
            }
            return true;
        }

        bool
        pattern_matches( const segmented_path& segments,
                         const std::string&    path )
        {
            NullParamCallback cb;
            return pattern_matches_impl( segments, path, cb );
        }

        bool
        pattern_matches( const segmented_path& segments,
                         const std::string&    path,
                         path_parameter_list&  params )
        {
            ParamExtractionCallback cb{ params };
            return pattern_matches_impl( segments, path, cb );
        }

        segmented_path
        parse_pattern( const std::string& pattern )
        {
            segmented_path             segments{};
            std::vector< std::string > param_names{};

            std::cmatch re_match{};
            std::regex  int_param_re( "<int(:([a-zA-Z][a-zA-Z0-9_]*))?>" );
            std::regex  str_param_re( "<string(:([a-zA-Z][a-zA-Z0-9_]*))?>" );

            std::string delim( 1, '/' );
            auto        it = boost::make_split_iterator(
                pattern, boost::first_finder( delim, boost::is_equal( ) ) );

            auto range = boost::make_iterator_range( it, decltype( it )( ) );
            for ( const auto& segment : range )
            {
                const char* cur_ptr = &( *segment.begin( ) );
                auto dist = std::distance( segment.begin( ), segment.end( ) );
                if ( segment.begin( ) == segment.end( ) )
                {
                    segments.emplace_back( empty_param{} );
                }
                else if ( std::regex_match( cur_ptr,
                                            cur_ptr + dist,
                                            re_match,
                                            int_param_re ) )
                {
                    std::string param_name{};
                    if ( re_match.size( ) == 3 )
                    {
                        param_name = re_match[ 2 ].str( );
                        if ( std::find( param_names.begin( ),
                                        param_names.end( ),
                                        param_name ) != param_names.end( ) )
                        {
                            throw std::runtime_error( "You cannot repeat a "
                                                      "parameter name in a url "
                                                      "pattern" );
                        }
                        param_names.emplace_back( param_name );
                    }
                    segments.emplace_back(
                        int_param{ std::move( param_name ) } );
                }
                else if ( std::regex_match( cur_ptr,
                                            cur_ptr + dist,
                                            re_match,
                                            str_param_re ) )
                {
                    std::string param_name{};
                    if ( re_match.size( ) == 3 )
                    {
                        param_name = re_match[ 2 ].str( );
                        if ( std::find( param_names.begin( ),
                                        param_names.end( ),
                                        param_name ) != param_names.end( ) )
                        {
                            throw std::runtime_error( "You cannot repeat a "
                                                      "parameter name in a url "
                                                      "pattern" );
                        }
                        param_names.emplace_back( param_name );
                    }
                    segments.emplace_back(
                        string_param{ std::move( param_name ) } );
                }
                else
                {
                    segments.emplace_back(
                        std::string( segment.begin( ), segment.end( ) ) );
                }
            };
            return segments;
        }
    } // namespace detail
} // namespace webxx