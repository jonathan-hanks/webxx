/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <webxx/detail/multipart.hh>
#include <algorithm>
#include <iostream>
#include <vector>

#include <boost/utility/string_view.hpp>

namespace
{
    std::string dashes{ "--" };
    std::string NL{ "\r\n" };
    std::string end_headers{ "\r\n\r\n" };
    std::string dashes_NL{ "--\r\n" };
    std::string semicolon{ ";" };

    struct PartHeader
    {
        std::string name{};
        std::string filename{};
        std::string content_type{};
    };

    struct multipart_header
    {
        std::string                                          name{};
        std::string                                          value{};
        std::vector< std::pair< std::string, std::string > > attributes;
    };

    template < typename It >
    It
    skip_ws( It cur, It end )
    {
        while ( cur != end && ( *cur == ' ' || *cur == '\t' ) )
        {
            ++cur;
        }
        return cur;
    }

    template < typename It1, typename It2 >
    bool
    seq_match( It1 start1, It1 end1, It2 start2, It2 end2 )
    {
        auto cur1 = start1;
        auto cur2 = start2;

        while ( cur1 != end1 && cur2 != end2 )
        {
            if ( *cur1 != *cur2 )
            {
                return false;
            }
            ++cur1;
            ++cur2;
        }
        return cur1 == end1 && cur2 == end2;
    }

    template < typename It >
    bool
    matchs( It cur, It end, const std::string& phrase )
    {
        auto phrase_end = phrase.end( );
        auto phrase_cur = phrase.begin( );

        while ( cur != end && phrase_cur != phrase_end )
        {
            if ( *cur != *phrase_cur )
            {
                return false;
            }
            ++cur;
            ++phrase_cur;
        }
        return phrase_cur == phrase_end;
    }

    template < typename It >
    It
    consume( It cur, std::size_t count )
    {
        std::advance( cur, count );
        return cur;
    }

    template < typename It >
    It
    match_and_consume( It cur, It end, const std::string& phrase )
    {
        if ( matchs( cur, end, phrase ) )
        {
            return consume( cur, phrase.size( ) );
        }
        throw std::runtime_error( "Parse error" );
    }

    template < typename It >
    It
    find_delimiter( It cur, It end, const std::string& delim )
    {
        for ( ; cur != end; ++cur )
        {
            if ( matchs( cur, end, delim ) )
            {
                return cur;
            }
        }
        return cur;
    }

    template < typename It >
    It
    parse_attr_value( It cur, It end, std::string& dest )
    {
        const std::string quote = "\"";
        cur = match_and_consume( cur, end, quote );

        std::string value;
        bool        escape = false;
        bool        done = false;
        while ( cur != end && !done )
        {
            if ( escape )
            {
                value.append( cur, cur + 1 );
                escape = false;
            }
            else
            {
                if ( *cur == '\\' )
                {
                    escape = true;
                }
                else if ( *cur == '\"' )
                {
                    done = true;
                }
                else
                {
                    value.append( cur, cur + 1 );
                }
            }
            ++cur;
        }
        if ( !done || escape )
        {
            throw std::runtime_error(
                "Bad parse, no closing quote on a attribute" );
        }

        dest.swap( value );
        return cur;
    }

    template < typename It >
    It
    parse_header_value( It cur, It end, std::string& dest )
    {
        cur = skip_ws( cur, end );
        auto        delim = find_delimiter( cur, end, semicolon );
        std::string value( cur, delim );
        dest.swap( value );
        if ( delim == end )
        {
            return end;
        }
        return consume( delim, semicolon.size( ) );
    }

    template < typename It >
    It
    parse_header_name( It                 cur,
                       It                 end,
                       const std::string& sep,
                       std::string&       dest )
    {
        auto delim = find_delimiter( cur, end, sep );
        if ( delim == end )
        {
            throw std::runtime_error(
                "Parse error, no delimiter when parsing name" );
        }
        std::string name( cur, delim );
        dest.swap( name );
        return consume( delim, sep.size( ) );
    }

    template < typename It >
    multipart_header
    parse_multipart_header( It cur, It end )
    {
        multipart_header header;
        std::string      debug( cur, end );

        cur = parse_header_name( cur, end, ":", header.name );
        cur = parse_header_value( cur, end, header.value );
        cur = skip_ws( cur, end );
        while ( cur != end )
        {
            cur = skip_ws( cur, end );
            std::string name;
            std::string value;
            cur = parse_header_name( cur, end, "=", name );
            cur = parse_attr_value( cur, end, value );
            header.attributes.emplace_back(
                std::make_pair( std::move( name ), std::move( value ) ) );
            if ( cur != end )
            {
                cur = match_and_consume( cur, end, semicolon );
            }
        }
        return header;
    }

    std::string
    find_header_attr( const std::vector< multipart_header >& headers,
                      const std::string&                     header_name,
                      const std::string&                     attr_name )
    {
        auto it = std::find_if(
            headers.begin( ),
            headers.end( ),
            [&header_name]( const multipart_header& header ) -> bool {
                return header.name == header_name;
            } );
        if ( it == headers.end( ) )
        {
            return "";
        }
        auto at_it = std::find_if(
            it->attributes.begin( ),
            it->attributes.end( ),
            [&attr_name]( const std::pair< std::string, std::string >& attr )
                -> bool { return attr.first == attr_name; } );
        if ( at_it == it->attributes.end( ) )
        {
            return "";
        }
        return at_it->second;
    }

    std::string
    find_header_value( const std::vector< multipart_header >& headers,
                       const std::string&                     header_name )
    {
        auto it = std::find_if(
            headers.begin( ),
            headers.end( ),
            [&header_name]( const multipart_header& header ) -> bool {
                return header.name == header_name;
            } );
        if ( it == headers.end( ) )
        {
            return "";
        }
        return it->value;
    }

    template < typename It >
    PartHeader
    parse_multipart_headers( It cur, It end )
    {
        PartHeader                      results;
        std::vector< multipart_header > headers;

        auto endln = find_delimiter( cur, end, NL );
        while ( cur != endln )
        {
            std::string debug( cur, endln );
            headers.emplace_back( parse_multipart_header( cur, endln ) );

            cur = endln;
            if ( cur != end )
            {
                cur = consume( cur, NL.size( ) );
            }
            endln = find_delimiter( cur, end, NL );
        }

        results.name =
            find_header_attr( headers, "Content-Disposition", "name" );
        results.filename =
            find_header_attr( headers, "Content-Disposition", "filename" );
        results.content_type = find_header_value( headers, "Content-Type" );
        return results;
    }

} // namespace

namespace webxx
{
    namespace detail
    {
        simple_optional< query_params_t >
        decode_multipart( const std::string& body,
                          const std::string& boundary_ )
        {
            query_params_t results;
            auto           body_cur = body.begin( );
            auto           body_end = body.end( );
            std::string    boundary = "--" + boundary_;
            std::string    end_block_boundary = "\r\n" + boundary;
            auto           validate = [body_end]( auto it ) {
                if ( it == body_end )
                {
                    throw std::runtime_error( "Parse error" );
                }
            };

            body_cur = match_and_consume( body_cur, body_end, boundary );
            std::string remaining( body_cur, body_end );
            while ( matchs( body_cur, body_end, NL ) )
            {
                body_cur = consume( body_cur, NL.size( ) );
                auto it = find_delimiter( body_cur, body_end, end_headers );
                validate( it );
                auto headers = parse_multipart_headers( body_cur, it );
                body_cur = it;
                body_cur = consume( body_cur, end_headers.size( ) );

                it = find_delimiter( body_cur, body_end, end_block_boundary );
                remaining = std::string( body_cur, body_end );
                validate( it );

                results[ headers.name ] = std::string( body_cur, it );
                body_cur = it;
                body_cur =
                    match_and_consume( body_cur, body_end, end_block_boundary );
                remaining = std::string( body_cur, body_end );
            }
            body_cur = match_and_consume( body_cur, body_end, dashes_NL );
            if ( body_cur != body_end )
            {
                throw std::runtime_error( "Parse error" );
            }

            return results;
        }
    } // namespace detail
} // namespace webxx