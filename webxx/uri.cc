/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <webxx/uri.hh>
#include <webxx/detail/hex.hh>

#include <algorithm>

#include <boost/algorithm/string/find_iterator.hpp>
#include <boost/algorithm/string/finder.hpp>

namespace webxx
{
    namespace detail
    {
        /*!
         * @brief Class to help parse url parameter strings a=b&c=d&...
         */
        class UriParameterParser
        {
        public:
            explicit UriParameterParser( query_params_t& dest ) : dest_{ dest }
            {
            }

            void
            operator( )(
                const boost::iterator_range< std::string::const_iterator >&
                    range )
            {
                if ( range.begin( ) == range.end( ) )
                {
                    return;
                }
                auto split = std::find( range.begin( ), range.end( ), '=' );
                if ( split == range.end( ) )
                {
                    auto key = urldecode(
                        std::string( range.begin( ), range.end( ) ) );
                    if ( !key )
                    {
                        return;
                    }
                    dest_[ *key ] = "";
                }
                else
                {
                    auto key =
                        urldecode( std::string( range.begin( ), split ) );
                    ++split;
                    auto val = urldecode( std::string( split, range.end( ) ) );
                    if ( !key || !val )
                    {
                        return;
                    }
                    dest_[ *key ] = *val;
                }
            }

        private:
            query_params_t& dest_;
        };

        query_params_t
        parse_params( const std::string& input )
        {
            query_params_t params{};
            std::string    delim( 1, '&' );
            auto           it = boost::make_split_iterator(
                input, boost::first_finder( delim, boost::is_equal( ) ) );
            auto               end = decltype( it )( );
            UriParameterParser parser( params );
            std::for_each( it, end, parser );
            return params;
        }
    } // namespace detail

    simple_optional< query_params_t >
    url_decode_params( const std::string& input )
    {
        return detail::parse_params( input );
    }

    simple_optional< uri_t >
    parse_uri( const std::string& input )
    {
        query_params_t params{};
        auto           path_end = input.find( '?' );

        simple_optional< std::string > path;

        if ( path_end != std::string::npos )
        {
            path = urldecode( input.substr( 0, path_end ) );
            params = detail::parse_params( input.substr( path_end + 1 ) );
        }
        else
        {
            path = urldecode( input );
        }

        if ( !path )
        {
            return webxx::none;
        }
        return uri_t{ *path, std::move( params ) };
    }

    simple_optional< std::string >
    urldecode( const std::string& input )
    {
        std::string output;
        output.reserve( input.size( ) );

        std::array< char, 2 > sub_buffer{};
        auto                  sub_dest = sub_buffer.end( );
        for ( const auto& ch : input )
        {
            if ( sub_dest != sub_buffer.end( ) )
            {
                *sub_dest = ch;
                ++sub_dest;
                if ( sub_dest == sub_buffer.end( ) )
                {
                    if ( !detail::is_hex_digit( sub_buffer[ 0 ] ) ||
                         !detail::is_hex_digit( sub_buffer[ 1 ] ) )
                    {
                        return webxx::none;
                    }
                    unsigned int val = detail::from_hex( sub_buffer[ 0 ] )
                        << static_cast< unsigned int >( 4 );
                    val |= detail::from_hex( sub_buffer[ 1 ] );
                    output.push_back( static_cast< char >( val ) );
                }
            }
            else if ( ch == '+' )
            {
                output.push_back( ' ' );
            }
            else if ( ch == '%' )
            {
                sub_dest = sub_buffer.begin( );
            }
            else
            {
                output.push_back( ch );
            }
        }
        if ( sub_dest != sub_buffer.end( ) )
        {
            return webxx::none;
        }
        return output;
    }
} // namespace webxx