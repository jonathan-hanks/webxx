/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef WEBXX_MIDDLEWARE_FILE_SESSION_HH
#define WEBXX_MIDDLEWARE_FILE_SESSION_HH

#include <webxx/middleware/session.hh>

#include <mutex>

namespace webxx
{
    namespace middleware
    {

        class FileSessionManager : public SessionManager
        {
        public:
            explicit FileSessionManager( const std::string& session_dir );
            ~FileSessionManager( ) override = default;

            Session create_session( ) override;

            simple_optional< Session > load( SessionId id ) override;

            void destroy( Session& s ) override;

            void save( Session& s ) override;

        private:
            void do_save( Session& s );

            bool exists( const SessionId& id );

            static std::string id_to_string( const SessionId& id );

            std::string session_dir_;
            std::mutex  lock_;
        };
    } // namespace middleware
} // namespace webxx

#endif // WEBXX_MIDDLEWARE_FILE_SESSION_HH
