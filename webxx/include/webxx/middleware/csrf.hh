/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef WEBXX_MIDDLEWARE_CSRF_HH
#define WEBXX_MIDDLEWARE_CSRF_HH

#include <string>
#include <webxx/detail/hex.hh>
#include <webxx/middleware/session.hh>
#include <webxx/req_resp.hh>
#include <bsd/stdlib.h>
#include <iostream>

namespace webxx
{
    namespace middleware
    {

        template < typename StringLike1, typename StringLike2 >
        bool
        safe_str_equal( const StringLike1& str1, const StringLike2& str2 )
        {
            if ( str1.size( ) != str2.size( ) )
            {
                return false;
            }
            int flag = 0;
            for ( std::size_t i = 0; i < str1.size( ); ++i )
            {
                flag |= str1[ i ] ^ str2[ i ];
            }
            return flag == 0;
        }

        extern std::string csrf_token( webxx::request_t& req );

        template < typename Handler >
        handler_type
        csrf_middleware( Handler&& h )
        {
            return [next = std::move( h )]( request_t& req ) -> response_ptr {
                const int  CSRF_SIZE = 24;
                request_t& req_ = const_cast< request_t& >( req );
                webxx::middleware::Session* session =
                    boost::any_cast< webxx::middleware::Session* >(
                        req_.app_data( ).at( "session" ) );
                if ( !session )
                {
                    throw std::runtime_error(
                        "The session middleware must be enabled "
                        "before the csrf middleware" );
                }
                JsonType csrf = session->data( ).get( "csrf", 0 );
                if ( !csrf.isString( ) )
                {
                    if ( req_.method( ) == webxx::VERB::POST )
                    {
                        return webxx::string_response( 400, "Bad Request" );
                    }

                    std::array< char, CSRF_SIZE + 1 > csrf_string{};
                    arc4random_buf(
                        reinterpret_cast< void* >( csrf_string.data( ) ),
                        csrf_string.size( ) - 1 );
                    csrf_string.back( ) = '\0';
                    csrf = csrf_string.data( );
                    session->data( )[ "csrf" ] = csrf;
                }
                bool reset_csrf = false;
                if ( req_.method( ) == webxx::VERB::POST )
                {
                    reset_csrf = true;
                    std::string csrf_raw_param;
                    const auto& opt_params = req_.post_params( );
                    if ( !opt_params )
                    {
                        return webxx::string_response( 400, "Bad Request" );
                    }
                    const webxx::query_params_t& params = *opt_params;
                    auto it = params.find( "csrf_token" );
                    if ( it == params.end( ) )
                    {
                        return webxx::string_response( 400, "Bad Request" );
                    }
                    csrf_raw_param = it->second;

                    std::cerr << "csrf_token passed as '" << csrf_raw_param
                              << "'\n";
                    std::cerr << "csrf_token passed as '"
                              << csrf_raw_param.c_str( ) << "'\n";
                    auto param_csrf =
                        webxx::detail::from_hex_str( csrf_raw_param );
                    if ( param_csrf )
                    {
                        if ( !safe_str_equal( csrf.asString( ), *param_csrf ) )
                        {
                            std::cerr << "Failed csrf\n";
                            std::cerr << "'" << csrf.asString( ) << "'\n'";
                            std::cerr << *param_csrf << "'\n";
                            return webxx::string_response( 400, "Bad Request" );
                        }
                    }
                    else
                    {
                        std::cerr << "Failed with uninitialized optional\n";
                        return webxx::string_response( 400, "Bad Request" );
                    }
                }
                auto result = next( req );
                if ( reset_csrf )
                {
                    session->data( )[ "csrf" ] = csrf;
                }
                webxx::middleware::Session* session2 =
                    boost::any_cast< webxx::middleware::Session* >(
                        req_.app_data( ).at( "session" ) );
                std::cerr << "csrf token is = " << csrf_token( req_ ) << "\n";
                if ( session != session2 )
                {
                    std::cerr << "The session changed\n";
                }
                return std::move( result );
            };
        }

    } // namespace middleware
} // namespace webxx
#endif // WEBXX_MIDDLEWARE_CSRF_HH
