/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WEBXX_MIDDLEWARE_COOKIE_HH
#define WEBXX_MIDDLEWARE_COOKIE_HH

#include <string>
#include <unordered_map>

#include <webxx/req_resp.hh>

namespace webxx
{

    namespace middleware
    {
        using cookie_jar = std::unordered_map< std::string, std::string >;

        cookie_jar extract_cookies( const webxx::request_t& req );

        template < typename Handler >
        webxx::handler_type
        cookie_middleware( Handler&& h )
        {
            return [next = std::move( h )](
                       webxx::request_t& req ) -> webxx::response_ptr {
                auto cookies = extract_cookies( req );
                req.app_data( )[ "cookies" ] = &cookies;
                auto resp = next( req );
                for ( const auto& cookie : cookies )
                {
                    std::ostringstream os;
                    os << cookie.first << "=" << cookie.second;
                    resp->headers( ).insert( "Set-Cookie", os.str( ) );
                }
                return resp;
            };
        }
    } // namespace middleware

} // namespace webxx

#endif // WEBXX_MIDDLEWARE_COOKIE_HH
