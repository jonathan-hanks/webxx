/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef WEBXX_MIDDLEWARE_SESSION_HH
#define WEBXX_MIDDLEWARE_SESSION_HH

#include <array>
#include <string>

#include <webxx/json.hh>
#include <webxx/req_resp.hh>
#include <webxx/simple_optional.hh>
#include <webxx/middleware/cookie.hh>

namespace webxx
{
    namespace middleware
    {

        using SessionId = std::array< unsigned char, 16 >;

        extern void randomize_session_id( SessionId& id );

        class SessionManager;

        class Session
        {
        public:
            static std::string id_as_string( const SessionId& id );

            static simple_optional< SessionId >
            id_from_string( const std::string& s );

            const SessionId&
            id( ) const noexcept
            {
                return id_;
            }

            const JsonType&
            data( ) const noexcept
            {
                return data_;
            }

            JsonType&
            data( ) noexcept
            {
                return data_;
            }

        private:
            explicit Session( const SessionId& id ) : id_{ id }, data_{} {};

            SessionId id_{};
            JsonType  data_{};

            friend class SessionManager;
        };

        class SessionManager
        {
        public:
            virtual ~SessionManager( ) = default;

            virtual Session create_session( ) = 0;

            virtual simple_optional< Session > load( SessionId id ) = 0;

            virtual void save( Session& s ) = 0;

            virtual void destroy( Session& s ) = 0;

        protected:
            static Session create_session( const SessionId& id );
            static void    clear_session( Session& s );
        };

        template < typename Handler >
        webxx::handler_type
        session_middleware( SessionManager& manager, Handler&& h )
        {
            return [&manager, next = std::move( h )](
                       const webxx::request_t& req ) -> webxx::response_ptr {
                webxx::request_t& req_ = const_cast< webxx::request_t& >( req );
                auto              cookies = boost::any_cast< cookie_jar* >(
                    req_.app_data( )[ "cookies" ] );
                auto cookie_it = cookies->find( "session_id" );

                webxx::simple_optional< Session > s = webxx::none;
                SessionId                         sid{};
                bool                              have_id = false;
                if ( cookie_it != cookies->end( ) )
                {
                    auto id_val = Session::id_from_string( cookie_it->second );
                    if ( id_val )
                    {
                        sid = *id_val;
                        have_id = true;
                    }
                }
                if ( have_id )
                {
                    s = manager.load( sid );
                }
                if ( !s )
                {
                    s = manager.create_session( );
                }
                if ( s )
                {
                    cookies->insert( std::make_pair(
                        "session_id", Session::id_as_string( s->id( ) ) ) );
                }
                else
                {
                    cookies->erase( "session_id" );
                }
                Session the_session = *s;
                req_.app_data( )[ "session" ] = &the_session;
                auto resp = next( req_ );
                manager.save( the_session );
                cookies->insert( std::make_pair(
                    "session_id",
                    Session::id_as_string( the_session.id( ) ) ) );
                return resp;
            };
        }
    } // namespace middleware
} // namespace webxx

#endif // WEBXX_MIDDLEWARE_SESSION_HH
