//
// Created by jonathan.hanks on 2/8/20.
//

#ifndef WEBXX_VERBS_HH
#define WEBXX_VERBS_HH

namespace webxx
{

    /*!
     * @brief http verbs that can constrain routes
     */
    enum class VERB
    {
        GET,
        POST,
        ALL, /// A special verb used to mark match any
    };
} // namespace webxx

#endif // WEBXX_VERBS_HH
