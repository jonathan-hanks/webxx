/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WEBXX_PATH_PARAM_HH
#define WEBXX_PATH_PARAM_HH

#include <string>
#include <vector>

namespace webxx
{
    /*!
     * @brief a container to hold parameters extracted from a path
     */
    class path_parameter_list
    {
    public:
        using size_type = std::size_t;
        using value_type = std::pair< std::string, std::string >;
        using const_iterator = std::vector< value_type >::const_iterator;

        path_parameter_list( ) = default;

        template < typename C >
        explicit path_parameter_list( C& container )
            : params_( container.begin( ), container.end( ) )
        {
            static_assert(
                std::is_same< typename C::value_type, value_type >::value,
                "Input containers value_type does not match mhy value_type" );
        }

        size_type
        size( ) const
        {
            return params_.size( );
        }

        const_iterator
        begin( ) const
        {
            return params_.cbegin( );
        }

        const_iterator
        end( ) const
        {
            return params_.cend( );
        }

        void
        push_back( std::string value, std::string key = "" )
        {
            params_.emplace_back( std::move( value ), std::move( key ) );
        }

        const value_type operator[]( size_type index ) const
        {
            return params_[ index ];
        }

        std::string
        value( size_type index ) const
        {
            return params_.at( index ).first;
        }

        std::string
        value( const std::string& name ) const
        {
            auto it = std::find_if( params_.begin( ),
                                    params_.end( ),
                                    [&name]( const value_type& val ) -> bool {
                                        return val.second == name;
                                    } );
            if ( it == params_.end( ) )
            {
                throw std::out_of_range(
                    "Could not find the specified parameter name" );
            }
            return it->first;
        }

        std::string
        name( size_type index ) const
        {
            return params_.at( index ).second;
        }

        void
        clear( )
        {
            params_.clear( );
        }

    private:
        std::vector< value_type > params_{};
    };
} // namespace webxx

#endif // WEBXX_PATH_PARAM_HH
