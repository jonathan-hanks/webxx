/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WEBXX_REQ_RESP_HH
#define WEBXX_REQ_RESP_HH

#include <string>

#include <boost/any.hpp>
#include <boost/beast/http.hpp>

#include <webxx/json.hh>
#include <webxx/path_param.hh>
#include <webxx/simple_optional.hh>
#include <webxx/uri.hh>
#include <webxx/verbs.hh>

namespace webxx
{
    namespace detail
    {
        struct simple_server_intl;
        struct request_loader;
    } // namespace detail

    /*!
     * @brief request information for the simple_server
     */
    class request_t
    {
        friend struct detail::request_loader;

    public:
        using HeadersType = boost::beast::http::fields;

        /*!
         * @brief construct an empty request
         */
        request_t( )
            : method_{ VERB::GET }, uri_{}, params_{}, version_{ 10 },
              keepalive_{ false }
        {
        }
        /*!
         * Construct a request
         * @param method The http method to use
         * @param uri the path
         * @param params parameters from the path (not query params)
         * @param body The request body
         * @param version HTTP version
         * @param keepalive Is keepalive requested
         */
        request_t( VERB                       method,
                   const uri_t&               uri,
                   const path_parameter_list& params,
                   std::string                body = "",
                   int                        version = 11,
                   bool                       keepalive = true )
            : method_{ method }, uri_{ uri }, params_{ params },
              body_{ std::move( body ) }, version_{ version },
              keepalive_{ keepalive }, json_{ webxx::none }, app_data_{}
        {
        }

        /*!
         * Construct a request
         * @param method The http method to use
         * @param uri the path
         * @param params parameters from the path (not query params)
         * @param headers the request headers
         * @param body The request body
         * @param version HTTP version
         * @param keepalive Is keepalive requested
         */
        request_t( VERB                       method,
                   const uri_t&               uri,
                   const path_parameter_list& params,
                   HeadersType                req_headers,
                   std::string                body = "",
                   int                        version = 11,
                   bool                       keepalive = true )
            : method_{ method }, uri_{ uri }, params_{ params },
              headers_{ std::move( req_headers ) }, body_{ std::move( body ) },
              version_{ version },
              keepalive_{ keepalive }, json_{ webxx::none }, app_data_{}
        {
        }
        ~request_t( ) = default;

        request_t( request_t&& other ) = default;
        request_t& operator=( request_t&& other ) = default;

        VERB
        method( ) const
        {
            return method_;
        }

        const uri_t&
        uri( ) const
        {
            return uri_;
        }

        const std::string&
        path( ) const
        {
            return uri_.path;
        }

        const path_parameter_list
        params( ) const
        {
            return params_;
        }

        int
        version( ) const
        {
            return version_;
        }

        bool
        keepalive( ) const
        {
            return keepalive_ && version_ > 10;
        }

        const std::string&
        body( ) const
        {
            return body_;
        }

        const simple_optional< JsonType >&
        json( ) const
        {
            return json_;
        }

        const simple_optional< webxx::query_params_t >&
        post_params( ) const
        {
            return query_params_;
        }

        const HeadersType
        headers( ) const
        {
            return headers_;
        }

        const std::unordered_map< std::string, boost::any >&
        app_data( ) const
        {
            return app_data_;
        }

        std::unordered_map< std::string, boost::any >&
        app_data( )
        {
            return app_data_;
        }

    private:
        VERB                                          method_;
        uri_t                                         uri_;
        path_parameter_list                           params_;
        std::string                                   body_;
        int                                           version_;
        bool                                          keepalive_;
        simple_optional< JsonType >                   json_;
        simple_optional< webxx::query_params_t >      query_params_;
        HeadersType                                   headers_;
        std::unordered_map< std::string, boost::any > app_data_;
    };

    class response_t;
    /*!
     * @brief responses are returned as smart pointers.
     */
    using response_ptr = std::unique_ptr< response_t >;

    using handler_type = std::function< response_ptr( request_t& ) >;

    using continuation_handler_t = std::function< void( std::ostream& ) >;
    using optional_continuation = simple_optional< continuation_handler_t >;

    /*!
     * @brief base class for the responses used by the simple_server
     */
    class response_t
    {
    public:
        response_t( ) : headers_{}
        {
        }
        virtual ~response_t( ){};

        virtual optional_continuation
        continuation( )
        {
            return webxx::none;
        }

        virtual int status_code( ) const = 0;

        virtual std::string body( ) = 0;

        request_t::HeadersType&
        headers( )
        {
            return headers_;
        }

        const request_t::HeadersType&
        headers( ) const
        {
            return headers_;
        }

    private:
        request_t::HeadersType headers_;
    };

    /*!
     * @brief a response given as a string
     */
    class simple_string_response_t : public response_t
    {
    public:
        template < typename StringLike >
        simple_string_response_t( int status_code, StringLike message )
            : status_code_{ status_code }, body_{ message }
        {
        }
        ~simple_string_response_t( ) override{};

        int
        status_code( ) const override
        {
            return status_code_;
        }

        std::string
        body( ) override
        {
            return body_;
        }

    private:
        int         status_code_;
        std::string body_;
    };

    /*!
     * A response that allows data streaming
     */
    class simple_continuation_response_t : public response_t
    {
    public:
        template < typename ContinuationHandler >
        simple_continuation_response_t( int                   status_code,
                                        ContinuationHandler&& handler )
            : status_code_{ status_code },
              continuation_(
                  [handler = std::forward< ContinuationHandler&& >( handler )](
                      std::ostream& os ) { handler( os ); } )
        {
        }
        std::string
        body( ) override
        {
            return "";
        }

        optional_continuation
        continuation( ) override
        {
            return continuation_;
        }

        int
        status_code( ) const override
        {
            return status_code_;
        }

    private:
        int                    status_code_;
        continuation_handler_t continuation_;
    };

    template < typename ContinuationHandler >
    response_ptr
    continuation_response( int status_code, ContinuationHandler&& handler )
    {
        return std::make_unique< simple_continuation_response_t >(
            status_code, std::move( handler ) );
    }

    /*!
     * @brief a helper function to quickly make a string response
     * @tparam StringLike
     * @param status_code
     * @param message
     * @return
     */
    template < typename StringLike >
    response_ptr
    string_response( int status_code, StringLike message )
    {
        return std::make_unique< simple_string_response_t >( status_code,
                                                             message );
    }
} // namespace webxx

#endif // WEBXX_REQ_RESP_HH
