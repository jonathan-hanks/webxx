/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SIMPLE_OPTIONAL_SIMPLE_OPTIONAL_HH
#define SIMPLE_OPTIONAL_SIMPLE_OPTIONAL_HH

#include <stdexcept>
#include <type_traits>

namespace webxx
{
    class bad_optional : public std::runtime_error
    {
    public:
        bad_optional( ) : runtime_error( "Accessing an empty optional" ){};
        ~bad_optional( ) override{};
    };

    struct none_t
    {
    };
    const none_t none;

    /*!
     * @brief A simple optional type container.
     * @details This was built to provide a simple optional container
     * that only allows moves from the container explicitly.
     * @tparam T The type that might be stored in the object.
     */
    template < typename T >
    class simple_optional
    {
    public:
        using value_type = T;

        simple_optional( ) = default;
        simple_optional( const none_t& n ){};

        simple_optional( const T& val )
        {
            new ( &storage_ ) T( val );
            initialized_ = true;
        }

        simple_optional( simple_optional&& other )
        {
            initialized_ = other.initialized_;
            if ( initialized_ )
            {
                new ( &storage_ ) T( other.move_out( ) );
            }
        }

        explicit simple_optional( T&& val )
        {
            new ( &storage_ ) T( std::move( val ) );
            initialized_ = true;
        }

        ~simple_optional( )
        {
            if ( initialized_ )
            {
                reinterpret_cast< T* >( &storage_ )->~T( );
            }
        }

        explicit operator bool( ) const
        {
            return initialized_;
        }

        simple_optional&
        operator=( const T& val )
        {
            if ( initialized_ )
            {
                reinterpret_cast< T* >( &storage_ )->~T( );
                initialized_ = false;
            }
            new ( &storage_ ) T( val );
            initialized_ = true;
            return *this;
        }

        simple_optional&
        operator=( const webxx::none_t& n ) noexcept
        {
            clear( );
            return *this;
        }

        simple_optional&
        operator=( const simple_optional& other )
        {
            if ( this != &other )
            {
                clear( );
                new ( &storage_ ) T( *other );
                initialized_ = true;
            }
            return *this;
        }

        simple_optional&
        operator=( simple_optional&& other )
        {
            if ( this != &other )
            {
                clear( );
                if ( other.initialized_ )
                {
                    new ( &storage_ ) T( std::move( other.move_out( ) ) );
                    initialized_ = true;
                }
            }
            return *this;
        }

        simple_optional&
        operator=( T&& val )
        {
            clear( );
            new ( &storage_ ) T( std::move( val ) );
            initialized_ = true;
            return *this;
        }

        const T& operator*( ) const
        {
            return *reinterpret_cast< const T* >( &storage_ );
        }

        T* operator->( )
        {
            return reinterpret_cast< T* >( &storage_ );
        }

        const T* operator->( ) const
        {
            return reinterpret_cast< T* >( &storage_ );
        }

        const T&
        at( ) const
        {
            if ( !initialized_ )
            {
                throw bad_optional( );
            }
            return *reinterpret_cast< const T* >( &storage_ );
        }

        T&
        at( )
        {
            if ( !initialized_ )
            {
                throw bad_optional( );
            }
            return *reinterpret_cast< T* >( &storage_ );
        }

        void
        clear( ) noexcept
        {
            if ( initialized_ )
            {
                reinterpret_cast< T* >( &storage_ )->~T( );
                initialized_ = false;
            }
        }

        T&&
        move_out( )
        {
            initialized_ = false;
            return std::move( *reinterpret_cast< T* >( &storage_ ) );
        }

    private:
        typename std::aligned_storage< sizeof( T ) >::type storage_{};
        bool initialized_{ false };
    };

} // namespace webxx

#endif // SIMPLE_OPTIONAL_SIMPLE_OPTIONAL_HH
