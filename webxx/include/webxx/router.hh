/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WEBXX_ROUTER_HH
#define WEBXX_ROUTER_HH

#include <string>
#include <unordered_map>
#include <boost/asio/buffer.hpp>
#include <boost/beast/core/flat_buffer.hpp>
#include <boost/variant.hpp>

#include <webxx/path_param.hh>
#include <webxx/simple_optional.hh>
#include <webxx/verbs.hh>

/*!
 * @brief A router, it matches input requests to handlers
 * @tparam Handler The type of the handler.
 */

namespace webxx
{
    namespace detail
    {
        struct string_param
        {
            std::string name{};
        };
        struct int_param
        {
            std::string name{};
        };
        struct empty_param
        {
        };
        using path_segment =
            boost::variant< std::string, string_param, int_param, empty_param >;
        using segmented_path = std::vector< path_segment >;

        segmented_path parse_pattern( const std::string& pattern );
        bool pattern_matches( const segmented_path&, const std::string& path );
        bool pattern_matches( const segmented_path&,
                              const std::string&   path,
                              path_parameter_list& params );
    } // namespace detail

    enum class legacy_evaluation
    {
        not_legacy,
        maybe_legacy,
        legacy,
    };

    class LegacyIO
    {
    public:
        LegacyIO() = default;
        virtual ~LegacyIO() = default;
        virtual void read(boost::asio::mutable_buffer dest) = 0;
        virtual std::size_t read_some(boost::asio::mutable_buffer dest) = 0;
        virtual void write(boost::asio::const_buffer src) = 0;
        virtual void close() = 0;
    };
    
    using legacy_evaluator = std::function<legacy_evaluation(boost::asio::const_buffer)>;
    using legacy_handler = std::function<void(boost::beast::flat_buffer&, LegacyIO&)>;

    template < typename Handler >
    class router_t
    {

    public:
        router_t( ) = default;

        /*!
         * @brief Add a route to the router
         * @param input the url pattern to match this handler with
         * @param handler the handler
         *
         * The pattern may be a string literal, or it may contain
         * parameter blocks.
         *
         *  - &lt;int&gt; an int parameter
         */
        void
        push_back( std::string input, Handler&& handler )
        {
            handlers_.emplace_back( VERB::ALL,
                                    detail::parse_pattern( input ),
                                    std::move( handler ) );
        }

        /*!
         * @brief add a route to the router, constrained by a HTTP verb
         * @param verb the http verb to constrain this match
         * @param input the url pattern to match this pattern with
         * @param handler the handler
         */
        void
        push_back( VERB verb, std::string input, Handler&& handler )
        {
            handlers_.emplace_back(
                verb, detail::parse_pattern( input ), std::move( handler ) );
        }

        void
        special( int status, Handler&& handler )
        {
            special_handlers_[ status ] = handler;
        }

        simple_optional< Handler >
        special( int status )
        {
            simple_optional< Handler > handler = webxx::none;
            auto                       it = special_handlers_.find( status );
            if ( it != special_handlers_.end( ) )
            {
                handler = it->second;
            }
            return handler;
        }

        /*!
         * @brief Given an input path, find a handler to match it
         * @param verb The request verb
         * @param pattern The input path
         * @return An optional for the matching handler
         */
        simple_optional< Handler >
        route( VERB                 verb,
               const std::string&   pattern,
               path_parameter_list* param_list = nullptr )
        {
            simple_optional< Handler > result = webxx::none;

            auto it = std::find_if(
                handlers_.begin( ),
                handlers_.end( ),
                [verb, &pattern]( const handler_mapping& m ) -> bool {
                    auto handler_verb = std::get< VERB >( m );

                    return ( verb == handler_verb ||
                             handler_verb == VERB::ALL ) &&
                        detail::pattern_matches(
                               std::get< detail::segmented_path >( m ),
                               pattern );
                } );
            if ( it == handlers_.end( ) )
            {
                return webxx::none;
            }
            result = std::get< Handler >( *it );
            if ( param_list )
            {
                path_parameter_list tmp_list{};
                detail::pattern_matches(
                    std::get< detail::segmented_path >( *it ),
                    pattern,
                    tmp_list );
                *param_list = std::move( tmp_list );
            }
            return result;
        }

        bool has_legacy_evaluator() const
        {
            return static_cast<bool>(legacy_evaluator_);
        }

        void setup_legacy(legacy_evaluator evaluator, legacy_handler handler)
        {
            legacy_evaluator_ = std::move(evaluator);
            legacy_handler_ = std::move(handler);
        }

        legacy_evaluation
        route_legacy(boost::beast::flat_buffer& input, LegacyIO& io)
        {
            auto eval = legacy_evaluator_(input.data());
            if (eval == legacy_evaluation::legacy )
            {
                legacy_handler_( input,  io);
                return legacy_evaluation::legacy;
            }
            return eval;
        }
    private:
        using handler_mapping =
            std::tuple< VERB, detail::segmented_path, Handler >;
        std::vector< handler_mapping >     handlers_{};
        std::unordered_map< int, Handler > special_handlers_{};
        legacy_evaluator legacy_evaluator_{};
        legacy_handler legacy_handler_{};
    };

} // namespace webxx

#endif // WEBXX_ROUTER_HH
