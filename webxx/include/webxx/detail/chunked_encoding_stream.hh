//
// Created by jonathan.hanks on 10/18/19.
//

#ifndef HTTP_FRAMEWORK_CHUNKED_ENCODING_STREAM_HH
#define HTTP_FRAMEWORK_CHUNKED_ENCODING_STREAM_HH

#include <cstdio>
#include <iostream>
#include <limits>

#include <boost/asio/buffer.hpp>
#include <boost/system/error_code.hpp>

namespace webxx
{

    namespace detail
    {

        /*!
         * @brief A stream buffer designed to let a client write an http chunked
         * encoding stream to an ostream
         * using a fixed size buffer and async IO to empty the buffer.
         * @tparam IOWriter The underlying IO object.
         */
        template < typename IOWriter >
        class chunked_encoding_stream_buf : public std::streambuf
        {
        public:
            chunked_encoding_stream_buf( IOWriter&                   io,
                                         boost::asio::mutable_buffer buf )
                : io_{ io }, main_buf_{ buf }, bufs_{}, closed_{ false }
            {
                static_assert( std::numeric_limits< long int >::max( ) >
                                   9999999999,
                               "Long int is too small on this platform" );
                if ( main_buf_.size( ) > 9999999999 || main_buf_.size( ) < 16 )
                {
                    throw std::runtime_error(
                        "The buffer is an invalid size for "
                        "the chunked_encoding_stream_buf, "
                        "16-9999999999 bytes please" );
                }
                bufs_[ 0 ] =
                    boost::asio::buffer( header_ptr( ), header_size( ) );
                bufs_[ 1 ] =
                    boost::asio::buffer( buffer_ptr( ), buffer_size( ) );
                bufs_[ 2 ] =
                    boost::asio::buffer( footer_ptr( ), footer_size( ) );
                boost::asio::buffer_copy( bufs_[ 2 ],
                                          boost::asio::buffer( "\r\n" ) );
                reset_pointers( );
            }

            ~chunked_encoding_stream_buf( ) override
            {
                try
                {
                    close_stream( );
                }
                catch ( ... )
                {
                }
            }

            /*!
             * @brief Force the closing of the buffer and closeout of the stream
             * in a context where exceptions can be seen.
             */
            void
            close_stream( )
            {
                if ( !closed_ )
                {
                    sync( );
                    empty_block( );
                    closed_ = true;
                }
            }

            /*!
             * @brief helper, to show if the backend considers itself closed
             * @return true if closed, else false
             */
            bool
            closed( ) const
            {
                return closed_;
            }

            /*!
             * @brief debugging helper
             * @return the amount of data left in the buffer before it must be
             * flushed.
             */
            std::size_t
            remaining_buffer( ) const
            {
                return epptr( ) - pptr( );
            }

        protected:
            std::streamsize
            xsputn( const char_type* s, std::streamsize count ) override
            {
                return basic_streambuf::xsputn( s, count );
            }

            int_type
            overflow( int_type ch ) override
            {
                if ( ch == traits_type::eof( ) )
                {
                    return traits_type::eof( );
                }
                *pptr( ) = ch;
                pbump( 1 );
                return ( sync( ) >= 0 ? ch : traits_type::eof( ) );

                return ch;
            }

        private:
            void
            reset_pointers( )
            {
                setp( reinterpret_cast< char* >( buffer_ptr( ) ),
                      reinterpret_cast< char* >( buffer_ptr( ) ) +
                          buffer_size( ) - 1 );
            }

            int
            sync( ) override
            {
                if ( closed_ )
                {
                    return -1;
                }
                if ( pbase( ) != pptr( ) )
                {
                    std::streamsize payload_size = pptr( ) - pbase( );
                    std::size_t     header_len = std::snprintf(
                        reinterpret_cast< char* >( header_ptr( ) ),
                        static_cast< size_t >( header_size( ) ),
                        "%lX\r\n",
                        static_cast< long int >( payload_size ) );
                    if ( header_len > header_size( ) )
                    {
                        closed_ = true;
                        return -1;
                    }
                    bufs_[ 0 ] =
                        boost::asio::buffer( header_ptr( ), header_len );
                    bufs_[ 1 ] =
                        boost::asio::buffer( buffer_ptr( ), payload_size );
                    std::cerr << "sending chunk of with a payload of "
                              << payload_size << " bytes\n";
                    auto ec = io_( bufs_ );
                    reset_pointers( );
                    if ( ec )
                    {
                        std::cerr << ec;
                        closed_ = true;
                        return -1;
                    }
                }
                return 0;
            }

            void*
            header_ptr( )
            {
                return main_buf_.data( );
            }

            std::size_t constexpr
                        header_size( ) const
            {
                return 12;
            }

            void*
            buffer_ptr( )
            {
                return reinterpret_cast< void* >(
                    reinterpret_cast< char* >( main_buf_.data( ) ) +
                    header_size( ) );
            }

            std::size_t
            buffer_size( ) const
            {
                return main_buf_.size( ) - header_size( ) - footer_size( );
            }

            void*
            footer_ptr( )
            {
                return reinterpret_cast< void* >(
                    reinterpret_cast< char* >( main_buf_.data( ) ) +
                    main_buf_.size( ) - footer_size( ) );
            }

            std::size_t constexpr
                        footer_size( ) const
            {
                return 2;
            }

            void
            empty_block( )
            {
                boost::asio::buffer_copy( bufs_[ 0 ],
                                          boost::asio::buffer( "0\r\n" ) );
                bufs_[ 0 ] = boost::asio::buffer( header_ptr( ), 3 );
                bufs_[ 1 ] = boost::asio::buffer( buffer_ptr( ), 0 );
                if ( io_( bufs_ ) )
                {
                    closed_ = true;
                    throw std::runtime_error( "Unable to send empty block" );
                }
            }

            IOWriter&                                    io_;
            boost::asio::mutable_buffer                  main_buf_;
            std::array< boost::asio::mutable_buffer, 3 > bufs_;
            bool                                         closed_;
        };
    } // namespace detail
} // namespace webxx
#endif // HTTP_FRAMEWORK_CHUNKED_ENCODING_STREAM_HH
