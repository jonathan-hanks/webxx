//
// Created by jonathan.hanks on 2/28/20.
//

#ifndef WEBXX_DETAIL_MULTIPART_HH
#define WEBXX_DETAIL_MULTIPART_HH

#include <string>
#include <webxx/simple_optional.hh>
#include <webxx/uri.hh>

namespace webxx
{
    namespace detail
    {
        template < typename StringLike >
        std::string
        extract_boundary_value( const StringLike& content_distribution )
        {
            const static std::string boundary = "boundary=";

            auto pos = content_distribution.find( boundary );
            if ( pos == StringLike::npos )
            {
                return "";
            }
            pos += boundary.size( );
            auto end = pos + 1;
            while ( end < content_distribution.size( ) &&
                    content_distribution[ end ] != ' ' )
            {
                ++end;
            }
            return content_distribution.substr( pos, end - pos );
        }

        simple_optional< query_params_t >
        decode_multipart( const std::string& body,
                          const std::string& boundary );
    } // namespace detail
} // namespace webxx
#endif // WEBXX_DETAIL_MULTIPART_HH
