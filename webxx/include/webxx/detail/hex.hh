/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef WEBXX_DETAIL_HEX_HH
#define WEBXX_DETAIL_HEX_HH

#include <string>
#include <webxx/simple_optional.hh>

namespace webxx
{
    namespace detail
    {
        extern bool is_hex_digit( char ch );

        extern unsigned int from_hex( int ch );

        extern char to_hex_nibble( int ch );

        template < typename StrLike >
        std::string
        to_hex_str( const StrLike& s )
        {
            std::string hex( s.size( ) * 2, ' ' );
            auto        dest = hex.begin( );
            for ( const auto& ch : s )
            {
                *dest = to_hex_nibble( ch >> 4 );
                ++dest;
                *dest = to_hex_nibble( ch & 0xf );
                ++dest;
            }
            return hex;
        }

        template < std::size_t N >
        std::string
        to_hex_str( const char ( &s )[ N ] )
        {
            std::string hex( ( N - 1 ) * 2, ' ' );
            auto        dest = hex.begin( );
            for ( std::size_t i = 0; i < ( N - 1 ); ++i )
            {
                const char ch = s[ i ];
                *dest = to_hex_nibble( ch >> 4 );
                ++dest;
                *dest = to_hex_nibble( ch & 0xf );
                ++dest;
            }
            return hex;
        }

        inline webxx::simple_optional< std::string >
        from_hex_str( const std::string& hex )
        {
            webxx::simple_optional< std::string > results = webxx::none;

            if ( hex.size( ) % 2 == 0 )
            {
                bool        valid{ true };
                std::string s( hex.size( ) / 2, ' ' );
                auto        dest = s.begin( );
                for ( int i = 0; i < hex.size( ); )
                {
                    int out;
                    int ch = hex[ i ];
                    if ( !is_hex_digit( ch ) )
                    {
                        valid = false;
                        break;
                    }
                    out = ( 0x0f & from_hex( ch ) ) << 4;
                    ++i;
                    ch = hex[ i ];
                    if ( !is_hex_digit( ch ) )
                    {
                        valid = false;
                        break;
                    }
                    out |= from_hex( ch ) & 0x0f;
                    ++i;
                    *dest = out;
                    ++dest;
                }
                if ( valid )
                {
                    results = s;
                }
            }
            return results;
        }

        template < typename StringLike, typename It >
        bool
        from_hex_str( StringLike hex, It dest )
        {
            webxx::simple_optional< std::string > results = webxx::none;

            if ( hex.size( ) % 2 != 0 )
            {
                return false;
            }

            for ( int i = 0; i < hex.size( ); )
            {
                int out;
                int ch = hex[ i ];
                if ( !is_hex_digit( ch ) )
                {
                    return false;
                }
                out = ( 0x0f & from_hex( ch ) ) << 4;
                ++i;
                ch = hex[ i ];
                if ( !is_hex_digit( ch ) )
                {
                    return false;
                }
                out |= from_hex( ch ) & 0x0f;
                ++i;
                *dest = out;
                ++dest;
            }

            return true;
        }
    } // namespace detail
} // namespace webxx
#endif // WEBXX_DETAIL_HEX_HH
