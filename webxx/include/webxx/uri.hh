/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WEBXX_URI_HH
#define WEBXX_URI_HH

#include <string>
#include <unordered_map>

#include <webxx/simple_optional.hh>

namespace webxx
{

    using query_params_t = std::unordered_map< std::string, std::string >;

    simple_optional< query_params_t >
    url_decode_params( const std::string& input );

    /*!
     * @brief represents a uri
     */
    struct uri_t
    {
        // http://host:port/path?param=val&parm=val
        //        std::string scheme;
        //        std::string server;
        //        int         port;
        std::string    path;
        query_params_t params;
    };

    /*!
     * @brief given a uri as a string, decode it into a uri structure
     * @param input input string
     * @return uri
     */
    simple_optional< uri_t > parse_uri( const std::string& input );

    /*!
     * @brief url decode the input
     * @param input input string
     * @return An optional containing the decoded string, or boost::none on
     * parse error
     */
    simple_optional< std::string > urldecode( const std::string& input );
} // namespace webxx

#endif // WEBXX_URI_HH
