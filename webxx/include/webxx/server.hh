/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef WEBXX_SERVER_HH
#define WEBXX_SERVER_HH

#include <webxx/router.hh>

#include <functional>

#include <boost/asio.hpp>

#include <webxx/req_resp.hh>

namespace webxx
{
    using io_context = boost::asio::io_context;

    class Server;

    struct Endpoint
    {
        Endpoint( ) = default;
        explicit Endpoint( std::string iface ) : interface{ std::move( iface ) }
        {
        }
        explicit Endpoint( std::uint16_t port_num ) : port{ port_num }
        {
        }
        Endpoint( std::string iface, std::uint16_t port_num )
            : interface{ std::move( iface ) }, port{ port_num } {};

        std::string   interface{};
        std::uint16_t port{ 9000 };
    };

    /*!
     * @brief a generic server.  It has no knowledge of the client protocol.
     * @detail The server simply manages the server socket and passes all
     * details of handling on to the Connection objects.
     */
    class Server
    {
    public:
        explicit Server( const Endpoint listen_endpoint = {}, int threads = 1 );

        void run( );

        router_t< handler_type >&
        router( )
        {
            return router_;
        }

    private:
        using acceptor_type = boost::asio::ip::tcp::acceptor;
        using context_ptr = std::shared_ptr< io_context >;
        using work_guard =
            boost::asio::executor_work_guard< io_context::executor_type >;

        void run_thread( int n );

        static std::vector< context_ptr > create_contexts( int count );

        static std::vector< work_guard >
        create_work_guard( std::vector< context_ptr >& ctxs );

        void accept_loop( );

        std::vector< context_ptr > contexts_;
        std::vector<
            boost::asio::executor_work_guard< io_context::executor_type > >
                                   work_;
        std::vector< std::thread > threads_;
        acceptor_type              acceptor_;
        router_t< handler_type >   router_;
    };
} // namespace webxx

#endif // WEBXX_SERVER_HH
