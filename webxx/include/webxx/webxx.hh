/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef WEBXX_WEBXX_HH
#define WEBXX_WEBXX_HH

/*!
 * @file webxx.hh
 * @brief webxx is a simple HTTP framework, providing basic servers, clients,
 * and routers. The desire is to be able to build a flexible base which can
 * quickly be built from, while still giving enough power to do work.
 *
 * @details The code is built around using boost::asio and boost::fiber.  The
 * combination gives a async model that can be expressed in linear looking code.
 */

#include <webxx/server.hh>

#endif // WEBXX_WEBXX_HH
