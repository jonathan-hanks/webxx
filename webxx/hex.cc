/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <webxx/detail/hex.hh>

namespace webxx
{
    namespace detail
    {
        bool
        is_hex_digit( char ch )
        {
            return ( ch >= '0' && ch <= '9' ) || ( ch >= 'a' && ch <= 'f' ) ||
                ( ch >= 'A' && ch <= 'F' );
        }

        unsigned int
        from_hex( int ch )
        {
            if ( ch >= '0' && ch <= '9' )
            {
                return ch - '0';
            }
            if ( ch >= 'a' && ch <= 'f' )
            {
                return 10 + ch - 'a';
            }
            if ( ch >= 'A' && ch <= 'F' )
            {
                return 10 + ch - 'A';
            }
            return 0;
        }

        char
        to_hex_nibble( int ch )
        {
            ch &= 0x0f;
            if ( ch < 10 )
            {
                return static_cast< char >( '0' + ch );
            }
            return static_cast< char >( ( ch - 10 ) + 'a' );
        }
    } // namespace detail
} // namespace webxx