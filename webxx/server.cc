/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <webxx/server.hh>

#include <iostream>
#include <memory>
#include <vector>

#include <boost/beast.hpp>
#include <boost/fiber/fiber.hpp>

#include <webxx/detail/chunked_encoding_stream.hh>
#include <webxx/detail/multipart.hh>

#include "round_robin.hpp"
#include "yield.hpp"

namespace webxx
{
    namespace detail
    {
        class AsyncLegacyIO: public LegacyIO
        {
        public:
            AsyncLegacyIO(boost::asio::ip::tcp::socket& s): s_{s}
            {

            }
            ~AsyncLegacyIO( ) override = default;

            void
            read( boost::asio::mutable_buffer dest ) override
            {
                boost::system::error_code ec;
                boost::asio::async_read(s_, dest, boost::fibers::asio::yield[ ec ]);
                if ( ec )
                {
                    throw ec;
                }
            }
            size_t
            read_some( boost::asio::mutable_buffer dest ) override
            {
                boost::system::error_code ec;
                auto bytes_read = s_.async_read_some(dest, boost::fibers::asio::yield[ ec ]);
                if ( ec )
                {
                    throw ec;
                }
                return bytes_read;
            }
            void
            write( boost::asio::const_buffer src ) override
            {
                boost::system::error_code ec;
                boost::asio::async_write(s_, src, boost::fibers::asio::yield[ ec ]);
                if ( ec )
                {
                    throw ec;
                }
            }
            void
            close( ) override
            {
                s_.close();
            }
        private:
            boost::asio::ip::tcp::socket& s_;
        };

        boost::asio::ip::tcp::endpoint
        make_endpoint( const webxx::Endpoint& endpoint )
        {
            if ( endpoint.interface.empty( ) )
            {
                return { boost::asio::ip::tcp::v4( ), endpoint.port };
            }
            return { boost::asio::ip::make_address( endpoint.interface ),
                     endpoint.port };
        }

        simple_optional< webxx::VERB >
        from_beast_verb( boost::beast::http::verb verb )
        {
            using beast_verb = boost::beast::http::verb;
            using http_verb = webxx::VERB;

            switch ( verb )
            {
            case beast_verb::get:
                return http_verb::GET;
            case beast_verb::post:
                return http_verb::POST;
            default:
                return webxx::none;
            }
            return webxx::none;
        }

        struct request_loader
        {
            //            void
            //            set_json( request_t& req, Json::Value doc )
            //            {
            //                req.json_ = doc;
            //            }

            void
            set_query_params( request_t&                        req,
                              simple_optional< query_params_t > params )
            {
                req.query_params_ = std::move( params );
            }

            void
            set_headers( request_t& req, request_t::HeadersType& headers )
            {
                req.headers_ = headers;
            }
        };

        /*!
         * @brief The model of a generic HTTP connection handler.
         * @detail The Connection manages the lifetime of the socket, the
         * parsing of the input the dispatching to a handler. The Connection
         * should be managed by a shared_ptr.  When all references to the
         * Connection are released, the connection is automatically closed down.
         */
        class Connection : public std::enable_shared_from_this< Connection >
        {
        public:
            using socket_type = boost::asio::ip::tcp::socket;

            /*!
             * @brief Used to create a connection
             * @param context The asio context needed to do network work
             * @param router The router to reference when a request is handled
             * @note This should be made private some way, to enforce the
             * creation of the connection with a shared ptr.
             */
            explicit Connection( io_context&               context,
                                 router_t< handler_type >& router )
                : s_{ context }, router_{ router }, read_buf_( )
            {
            }

            ~Connection( )
            {
                std::cerr << "Connection closing" << std::endl;
            }

            /*!
             * @brief Retrieve the network socket
             * @return the socket
             */
            socket_type&
            socket( )
            {
                return s_;
            }

            /*!
             * @brief start the connection, it must have a valid socket.
             */
            void
            run( )
            {
                read_request( );
            }

        private:
            void
            read_request( )
            {
                bool                      buffer_has_legacy_content{false};
                boost::system::error_code ec;

                if (router_.has_legacy_evaluator())
                {
                    AsyncLegacyIO io{s_};
                    legacy_evaluation eval = legacy_evaluation::not_legacy;
                    do
                    {
                        std::size_t length = s_.async_read_some(
                            read_buf_.prepare( 1024 ),
                            boost::fibers::asio::yield[ ec ] );
                        if ( ec )
                        {
                            return;
                        }
                        read_buf_.commit( length );
                        buffer_has_legacy_content = true;
                        eval = router_.route_legacy(read_buf_, io);
                        if (eval == legacy_evaluation::legacy )
                        {
                            if (!s_.is_open())
                            {
                                return;
                            }
                            eval = legacy_evaluation::maybe_legacy;
                        }
                    } while (eval == legacy_evaluation::maybe_legacy );
                }
                boost::beast::http::request_parser<
                    boost::beast::http::string_body >
                    parser;
                parser.eager( true );
                do
                {
                    boost::system::error_code ec;
                    if (!buffer_has_legacy_content )
                    {
                        std::size_t length = s_.async_read_some(
                            read_buf_.prepare( 1024 ),
                            boost::fibers::asio::yield[ ec ] );
                        if ( ec )
                        {
                            return;
                        }
                        read_buf_.commit( length );
                    }
                    buffer_has_legacy_content = false;
                    ec.clear( );
                    auto bytes_used = parser.put( read_buf_.data( ), ec );
                    std::string tmp( (char*)read_buf_.data( ).data( ),
                                     read_buf_.data( ).size( ) );
                    std::cout << "received '" << tmp << '"' << std::endl;
                    if ( ec == boost::beast::http::error::need_more )
                    {
                        ec = {};
                    }
                    if ( ec )
                    {
                        return;
                    }
                    read_buf_.consume( bytes_used );
                } while ( !parser.is_done( ) );
                msg_ = parser.get( );

                dispatch_read( build_request( parser ) );
            }

            std::pair< request_t, handler_type >
            build_request( boost::beast::http::request_parser<
                           boost::beast::http::string_body >& parser )
            {
                simple_optional< VERB > verb =
                    from_beast_verb( msg_.method( ) );
                if ( !verb )
                {
                    return std::make_pair( request_t{}, bad_request( ) );
                }
                std::string target( msg_.target( ).begin( ),
                                    msg_.target( ).end( ) );
                auto        uri = parse_uri( target );
                if ( !uri )
                {
                    return std::make_pair( request_t{}, bad_request( ) );
                }
                uri_t&              cur_uri = uri.at( );
                path_parameter_list path_params;
                auto                opt_handler =
                    router_.route( *verb, cur_uri.path, &path_params );
                if ( !opt_handler )
                {
                    return std::make_pair( request_t{}, not_found( ) );
                }

                auto&       req = parser.get( );
                std::string body{ req.body( ) };
                std::string content_type = "";

                request_t simple_req{
                    *verb, cur_uri, path_params, parser.get( ).body( )
                };
                add_headers_to_request( simple_req, req.base( ) );
                auto ct_it = req.base( ).find( "Content-Type" );
                if ( ct_it != req.base( ).end( ) )
                {
                    content_type = std::string( ct_it->value( ) );
                    add_content_to_request( simple_req, body, content_type );
                    auto params = webxx::url_decode_params( body );
                    for ( const auto& entry : *params )
                    {
                        std::cout << entry.first << ": " << entry.second
                                  << "\n";
                    }
                }
                return std::make_pair( std::move( simple_req ),
                                       opt_handler.move_out( ) );
            }

            void
            dispatch_read( std::pair< request_t, handler_type > inputs )
            {
                static std::string failsafe_500{ "HTTP/1.0 500 Error\r\n"
                                                 "Content-Type: text/plain\r\n"
                                                 "Content-Length: 21\r\n"
                                                 "Connection: close\r\n\r\n"
                                                 "Internal Server Error" };
                auto write = [this]( auto buf ) -> boost::system::error_code {
                    boost::system::error_code ec;

                    boost::asio::async_write(
                        this->socket( ),
                        buf,
                        boost::fibers::asio::yield[ ec ] );
                    return ec;
                };

                namespace http = boost::beast::http;

                request_t&    cur_request = inputs.first;
                handler_type& cur_handler = inputs.second;

                boost::system::error_code ec{};
                std::string               header_buf{};
                optional_continuation     continuation{ webxx::none };
                response_ptr              response = nullptr;
                try
                {
                    auto version = cur_request.version( );
                    if ( version < 10 )
                    {
                        version = 10;
                    }
                    else if ( version > 11 )
                    {
                        version = 11;
                    }
                    std::cerr << "req - version " << version << "\n";

                    try
                    {
                        response = cur_handler( cur_request );
                    }
                    catch ( ... )
                    {
                        auto new_handler = router_.special( 500 );
                        if ( new_handler )
                        {
                            response = ( *new_handler )( cur_request );
                        }
                        else
                        {
                            throw;
                        }
                    }

                    continuation = response->continuation( );
                    http::response< http::string_body > res(
                        static_cast< http::status >( response->status_code( ) ),
                        version );
                    for ( const auto& hdr : response->headers( ) )
                    {
                        res.insert( hdr.name_string( ), hdr.value( ) );
                    }
                    res.set( http::field::server, BOOST_BEAST_VERSION_STRING );
                    res.keep_alive( cur_request.keepalive( ) );
                    if ( response->headers( ).find(
                             http::field::content_type ) ==
                         response->headers( ).end( ) )
                    {
                        res.set( http::field::content_type, "text/html" );
                    }

                    if ( continuation )
                    {
                        std::cout << "Have a continuation, requesting chunked "
                                     "output\n";
                        res.chunked( true );
                        // res.prepare_payload();
                    }
                    else
                    {
                        std::cout << "Regular output\n";
                        res.chunked( false );
                        res.content_length( response->body( ).size( ) );
                    }
                    for ( const auto& hdr : res.base( ) )
                    {
                        std::cout << "r " << hdr.name_string( ) << ": "
                                  << std::string( hdr.value( ) ) << "\n";
                    }

                    {
                        ec.clear( );

                        http::response_serializer< http::string_body > sr{
                            res
                        };
                        sr.split( true );

                        std::cerr << "calling write_header - chunked = "
                                  << res.chunked( ) << "\n";
                        do
                        {
                            bool error{ false };
                            sr.next( ec,
                                     [&sr, &error, this, &write](
                                         boost::system::error_code& ec,
                                         const auto&                buffer ) {
                                         ec.assign( 0, ec.category( ) );
                                         boost::system::error_code wec =
                                             write( buffer );
                                         if ( wec )
                                         {
                                             error = true;
                                         }
                                         sr.consume( boost::asio::buffer_size(
                                             buffer ) );
                                     } );
                            if ( error )
                            {
                                return;
                            }
                        } while ( !ec && !sr.is_header_done( ) );
                    }
                }
                catch ( ... )
                {
                    write( boost::asio::buffer( failsafe_500 ) );
                    return;
                }

                if ( continuation )
                {
                    std::array< char, 64 * 1024 > continuation_buffer{};
                    detail::chunked_encoding_stream_buf< decltype( write ) >
                                 buf( write,
                             boost::asio::buffer( continuation_buffer ) );
                    std::ostream os( &buf );
                    ( *continuation )( os );
                    os.flush( );
                }
                else
                {
                    if ( !response->body( ).empty( ) )
                    {
                        if ( write( boost::asio::buffer( response->body( ) ) ) )
                        {
                            return;
                        }
                    }
                }
                if ( cur_request.keepalive( ) )
                {
                    // io.resume_reading( );
                }
            }

            static void
            add_headers_to_request( request_t&              req,
                                    request_t::HeadersType& headers )
            {
                detail::request_loader loader;
                loader.set_headers( req, headers );
            }

            static void
            add_content_to_request( request_t&         req,
                                    const std::string& body,
                                    const std::string& content_type )
            {
                detail::request_loader loader;
                //                if ( content_type == "application/json" )
                //                {
                //                    Json::Value                         val;
                //                    Json::CharReaderBuilder builder;
                //                    std::unique_ptr< Json::CharReader >
                //                    reader(
                //                        builder.newCharReader( ) );
                //                    if ( reader->parse( body.data( ),
                //                                        body.data( ) +
                //                                        body.size( ), &val,
                //                                        nullptr ) )
                //                    {
                //                        loader.set_json( req, val );
                //                    }
                //                }
                //                else
                if ( content_type == "application/x-www-form-urlencoded" ||
                     content_type ==
                         "application/x-www-form-urlencoded; charset=UTF-8" )
                {
                    loader.set_query_params( req,
                                             webxx::url_decode_params( body ) );
                }
                if ( content_type.find( "multipart/form-data" ) == 0 )
                {
                    auto boundary =
                        detail::extract_boundary_value( content_type );
                    if ( !boundary.empty( ) )
                    {
                        loader.set_query_params(
                            req, detail::decode_multipart( body, boundary ) );
                    }
                }
            }

            static handler_type
            bad_request( )
            {
                return []( request_t& ) -> response_ptr {
                    return webxx::string_response( 400, "Bad Request" );
                };
            }

            static handler_type
            not_found( )
            {
                return []( request_t& ) -> response_ptr {
                    return webxx::string_response( 404, "Not Found" );
                };
            }

            boost::asio::ip::tcp::socket s_;
            router_t< handler_type >&    router_;
            boost::beast::flat_buffer    read_buf_;
            boost::beast::http::request< boost::beast::http::string_body > msg_;
            boost::string_view msg_path_;
        };

    } // namespace detail

    Server::Server( const Endpoint listen_endpoint, int threads )
        : contexts_( create_contexts( threads ) ),
          work_( create_work_guard( contexts_ ) ), threads_( threads ),
          acceptor_( *( contexts_[ 0 ] ),
                     detail::make_endpoint( listen_endpoint ) )
    {
    }

    void
    Server::run( )
    {
        for ( int i = 1; i < contexts_.size( ); ++i )
        {
            threads_[ i ] =
                std::thread( [this, i]( ) { this->run_thread( i ); } );
        }
        run_thread( 0 );
    }

    void
    Server::run_thread( int n )
    {
        boost::fibers::use_scheduling_algorithm<
            boost::fibers::asio::round_robin >( contexts_[ n ] );

        // thread 0 runs the accept loop
        if ( n == 0 )
        {
            boost::fibers::fiber( [this]( ) { accept_loop( ); } ).detach( );
        }
        contexts_[ n ]->run( );
    }

    std::vector< Server::context_ptr >
    Server::create_contexts( int count )
    {
        std::vector< context_ptr > ctxs;
        ctxs.reserve( count );
        for ( auto i = 0; i < count; ++i )
        {
            ctxs.emplace_back( std::make_shared< io_context >( ) );
        }
        return ctxs;
    }

    std::vector< Server::work_guard >
    Server::create_work_guard( std::vector< context_ptr >& ctxs )
    {
        std::vector< work_guard > guards;
        guards.reserve( ctxs.size( ) );
        std::for_each(
            ctxs.begin( ), ctxs.end( ), [&guards]( context_ptr& ctx ) {
                guards.emplace_back( boost::asio::make_work_guard( *ctx ) );
            } );
        return guards;
    }

    void
    Server::accept_loop( )
    {
        int selector = 0;
        int max = contexts_.size( );
        while ( true )
        {
            auto conn = std::make_shared< detail::Connection >(
                *contexts_[ selector ], router_ );
            boost::system::error_code ec;
            acceptor_.async_accept( conn->socket( ),
                                    boost::fibers::asio::yield[ ec ] );
            if ( ec )
            {
                throw boost::system::system_error( ec );
            }

            contexts_[ selector ]->post( [conn = std::move( conn )]( ) mutable {
                try
                {
                    boost::fibers::fiber(
                        [conn = std::move( conn )]( ) mutable {
                            conn->run( );
                        } )
                        .detach( );
                }
                catch ( ... )
                {
                }
            } );

            selector++;
            selector %= max;
        }
    }

} // namespace webxx