//
// Created by jonathan.hanks on 9/23/19.
//
#include <webxx/middleware/file_sessions.hh>
#include <webxx/detail/hex.hh>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

namespace filesystem = boost::filesystem;

namespace webxx
{
    namespace middleware
    {

        FileSessionManager::FileSessionManager( const std::string& session_dir )
            : session_dir_{ session_dir }, lock_{}
        {
            if ( !filesystem::exists( session_dir_ ) )
            {
                filesystem::create_directory( session_dir_ );
            }
        }

        Session
        FileSessionManager::create_session( )
        {
            SessionId                      id;
            std::unique_lock< std::mutex > l_( lock_ );

            while ( true )
            {
                randomize_session_id( id );
                if ( !exists( id ) )
                {
                    break;
                }
            }
            Session results = SessionManager::create_session( id );
            do_save( results );
            return results;
        }

        simple_optional< Session >
        FileSessionManager::load( SessionId id )
        {
            simple_optional< Session > result = webxx::none;

            std::unique_lock< std::mutex > l_( lock_ );
            if ( exists( id ) )
            {
                filesystem::path p =
                    filesystem::path( session_dir_ ) / id_to_string( id );
                filesystem::ifstream in( p );
                Session              s = SessionManager::create_session( id );
                in >> s.data( );
                result = s;
            }
            return result;
        }

        void
        FileSessionManager::destroy( Session& s )
        {
            std::unique_lock< std::mutex > l_( lock_ );
            if ( exists( s.id( ) ) )
            {
                filesystem::path p =
                    filesystem::path( session_dir_ ) / id_to_string( s.id( ) );
                filesystem::remove( p );
            }
            clear_session( s );
        }

        void
        FileSessionManager::save( webxx::middleware::Session& s )
        {
            std::unique_lock< std::mutex > l_( lock_ );
            do_save( s );
        }

        void
        FileSessionManager::do_save( Session& s )
        {
            filesystem::path p =
                filesystem::path( session_dir_ ) / id_to_string( s.id( ) );
            filesystem::ofstream fs( p );
            fs << s.data( );
        }

        bool
        FileSessionManager::exists( const SessionId& id )
        {
            filesystem::path p =
                filesystem::path( session_dir_ ) / id_to_string( id );
            return filesystem::exists( p );
        }

        std::string
        FileSessionManager::id_to_string(
            const webxx::middleware::SessionId& id )
        {
            std::string hex( id.size( ) * 2, ' ' );
            auto        dest = hex.begin( );
            for ( const auto& ch : id )
            {
                *dest = webxx::detail::to_hex_nibble( ch >> 4 );
                ++dest;
                *dest = webxx::detail::to_hex_nibble( ch & 0xf );
                ++dest;
            }
            return hex;
        }
    } // namespace middleware
} // namespace webxx