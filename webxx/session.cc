/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <webxx/middleware/session.hh>
#include <webxx/detail/hex.hh>

#include <bsd/stdlib.h>

namespace webxx
{
    namespace middleware
    {
        void
        randomize_session_id( SessionId& s )
        {
            ::arc4random_buf( reinterpret_cast< void* >( s.data( ) ),
                              s.size( ) );
        }

        std::string
        Session::id_as_string( const SessionId& id )
        {
            std::string hex( id.size( ) * 2, ' ' );
            auto        dest = hex.begin( );
            for ( const auto& ch : id )
            {
                *dest = webxx::detail::to_hex_nibble( ch >> 4 );
                ++dest;
                *dest = webxx::detail::to_hex_nibble( ch & 0xf );
                ++dest;
            }
            return hex;
        }

        simple_optional< SessionId >
        Session::id_from_string( const std::string& s )
        {
            simple_optional< SessionId > results = webxx::none;
            bool                         valid{ true };
            SessionId                    id{};
            if ( s.size( ) == id.size( ) * 2 )
            {
                auto dest = id.begin( );
                for ( int i = 0; i < s.size( ); )
                {
                    int out;
                    int ch = s[ i ];
                    if ( !webxx::detail::is_hex_digit( ch ) )
                    {
                        valid = false;
                        break;
                    }
                    out = ( 0x0f & webxx::detail::from_hex( ch ) ) << 4;
                    ++i;
                    ch = s[ i ];
                    if ( !webxx::detail::is_hex_digit( ch ) )
                    {
                        valid = false;
                        break;
                    }
                    out |= webxx::detail::from_hex( ch ) & 0x0f;
                    ++i;
                    *dest = out;
                    ++dest;
                }
                if ( valid )
                {
                    results = id;
                }
            }
            return results;
        }

        Session
        SessionManager::create_session( const SessionId& id )
        {
            return Session( id );
        }

        void
        SessionManager::clear_session( Session& s )
        {
            std::fill( s.id_.begin( ), s.id_.end( ), 0 );
            s.data_ = Json::Value{};
        }
    } // namespace middleware
} // namespace webxx