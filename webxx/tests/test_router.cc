//
// Created by jonathan.hanks on 8/29/19.
//
#include "webxx/router.hh"

#include "catch.hpp"

#include <functional>

TEST_CASE( "Test the parse_pattern call, simple test" )
{
    std::string                   test = "/abc/def/";
    webxx::detail::segmented_path components =
        webxx::detail::parse_pattern( test );

    REQUIRE( components.size( ) == 4 );

    REQUIRE_NOTHROW(
        boost::get< webxx::detail::empty_param >( components[ 0 ] ) );
    REQUIRE_NOTHROW( boost::get< std::string >( components[ 1 ] ) );
    REQUIRE_NOTHROW( boost::get< std::string >( components[ 2 ] ) );
    REQUIRE_NOTHROW(
        boost::get< webxx::detail::empty_param >( components[ 3 ] ) );
}

TEST_CASE( "Test the parse_pattern call, more advanced test" )
{
    std::string                   test = "/abc/<int>/def/<int:named>";
    webxx::detail::segmented_path components =
        webxx::detail::parse_pattern( test );

    REQUIRE( components.size( ) == 5 );

    REQUIRE_NOTHROW(
        boost::get< webxx::detail::empty_param >( components[ 0 ] ) );
    REQUIRE_NOTHROW( boost::get< std::string >( components[ 1 ] ) );
    REQUIRE_NOTHROW(
        boost::get< webxx::detail::int_param >( components[ 2 ] ) );
    REQUIRE_NOTHROW( boost::get< std::string >( components[ 3 ] ) );
    REQUIRE_NOTHROW(
        boost::get< webxx::detail::int_param >( components[ 4 ] ) );
}

TEST_CASE( "Test that segmented_paths can be matched" )
{
    webxx::detail::segmented_path segments{
        webxx::detail::empty_param{},
        "abc",
        webxx::detail::int_param{},
        webxx::detail::empty_param{},
    };
    REQUIRE( webxx::detail::pattern_matches( segments, "/abc/42/" ) );
    REQUIRE( !webxx::detail::pattern_matches( segments, "abc/42/" ) );
}

TEST_CASE( "You can create a router" )
{
    webxx::router_t< std::function< void( ) > > r;
}

TEST_CASE( "You can push_back routes to a router" )
{
    using handler_t = std::function< void( ) >;

    webxx::router_t< handler_t > r{};

    r.push_back( "/abc/", []( ) {} );
    r.push_back( "/def/", []( ) {} );
}

TEST_CASE( "Routes can specify http verbs to limit their scope" )
{
    using handler_t = std::function< void( ) >;

    webxx::router_t< handler_t > r{};

    r.push_back( webxx::VERB::GET, "/abc/", []( ) {} );
    r.push_back( webxx::VERB::POST, "/def/", []( ) {} );
}

TEST_CASE( "You can do routing" )
{
    using handler_t = std::function< int( ) >;

    webxx::router_t< handler_t > r{};

    auto GET = webxx::VERB::GET;
    auto POST = webxx::VERB::POST;

    r.push_back( "/abc/", []( ) -> int { return 1; } );
    r.push_back( "/def/", []( ) -> int { return 2; } );
    r.push_back( "/Abc/def/hello", []( ) -> int { return 3; } );

    auto handler = r.route( GET, "/abc/" );
    REQUIRE( static_cast< bool >( handler ) );
    REQUIRE( ( *handler )( ) == 1 );

    handler = r.route( POST, "/abc/" );
    REQUIRE( static_cast< bool >( handler ) );
    REQUIRE( ( *handler )( ) == 1 );

    handler = r.route( GET, "/def/" );
    REQUIRE( static_cast< bool >( handler ) );
    REQUIRE( ( *handler )( ) == 2 );

    handler = r.route( POST, "/def/" );
    REQUIRE( static_cast< bool >( handler ) );
    REQUIRE( ( *handler )( ) == 2 );

    handler = r.route( GET, "/Abc/def/hello" );
    REQUIRE( static_cast< bool >( handler ) );
    REQUIRE( ( *handler )( ) == 3 );

    handler = r.route( POST, "/Abc/def/hello" );
    REQUIRE( static_cast< bool >( handler ) );
    REQUIRE( ( *handler )( ) == 3 );

    handler = r.route( GET, "/ghi/" );
    REQUIRE( !static_cast< bool >( handler ) );

    handler = r.route( POST, "/ghi/" );
    REQUIRE( !static_cast< bool >( handler ) );
}

TEST_CASE( "You can do routing with verbs" )
{
    using handler_t = std::function< int( ) >;

    webxx::router_t< handler_t > r{};

    auto GET = webxx::VERB::GET;
    auto POST = webxx::VERB::POST;

    r.push_back( GET, "/abc/", []( ) -> int { return 1; } );
    r.push_back( POST, "/def/", []( ) -> int { return 2; } );
    r.push_back( GET, "/Abc/def/hello", []( ) -> int { return 3; } );

    auto handler = r.route( GET, "/abc/" );
    REQUIRE( static_cast< bool >( handler ) );
    REQUIRE( ( *handler )( ) == 1 );

    handler = r.route( POST, "/abc/" );
    REQUIRE( !static_cast< bool >( handler ) );

    handler = r.route( GET, "/def/" );
    REQUIRE( !static_cast< bool >( handler ) );

    handler = r.route( POST, "/def/" );
    REQUIRE( static_cast< bool >( handler ) );
    REQUIRE( ( *handler )( ) == 2 );

    handler = r.route( GET, "/Abc/def/hello" );
    REQUIRE( static_cast< bool >( handler ) );
    REQUIRE( ( *handler )( ) == 3 );

    handler = r.route( POST, "/Abc/def/hello" );
    REQUIRE( !static_cast< bool >( handler ) );

    handler = r.route( GET, "/ghi/" );
    REQUIRE( !static_cast< bool >( handler ) );

    handler = r.route( POST, "/ghi/" );
    REQUIRE( !static_cast< bool >( handler ) );
}

TEST_CASE( "You can do routing with parameters" )
{
    using handler_t = std::function< int( ) >;

    webxx::router_t< handler_t > r{};

    auto GET = webxx::VERB::GET;
    auto POST = webxx::VERB::POST;

    r.push_back( "/abc/<int>/", []( ) -> int { return 1; } );
    r.push_back( "<int>", []( ) -> int { return 2; } );
    r.push_back( "/Abc/<int>/hello", []( ) -> int { return 3; } );

    std::vector< std::pair< std::string, int > > test_cases = {
        { "/abc/3/", 1 },
        { "/abc/0/", 1 },
        { "/abc/-1/", 1 },
        { "-1", 2 },
        { "0", 2 },
        { "4", 2 },
        { "/Abc/-1/hello", 3 },
        { "/Abc/0/hello", 3 },
        { "/Abc/42/hello", 3 },
    };
    for ( const auto& test_case : test_cases )
    {
        auto handler = r.route( GET, test_case.first );
        REQUIRE( static_cast< bool >( handler ) );
        REQUIRE( ( *handler )( ) == test_case.second );
    }

    std::vector< std::string > failure_tests = {
        "/abc/<int>/",
        "<int>",
        "/Abc/<int>/hello",
    };
    for ( const auto& test_case : failure_tests )
    {
        auto handler = r.route( GET, test_case );
        REQUIRE( !static_cast< bool >( handler ) );
    }
}

TEST_CASE( "You can do routing with named parameters" )
{
    using handler_t = std::function< int( ) >;

    webxx::router_t< handler_t > r{};

    auto GET = webxx::VERB::GET;
    auto POST = webxx::VERB::POST;

    r.push_back( "/abc/<int:val>/", []( ) -> int { return 1; } );
    r.push_back( "<int:thing>", []( ) -> int { return 2; } );
    r.push_back( "/Abc/<int:var>/hello", []( ) -> int { return 3; } );

    std::vector< std::pair< std::string, int > > test_cases = {
        { "/abc/3/", 1 },
        { "/abc/0/", 1 },
        { "/abc/-1/", 1 },
        { "-1", 2 },
        { "0", 2 },
        { "4", 2 },
        { "/Abc/-1/hello", 3 },
        { "/Abc/0/hello", 3 },
        { "/Abc/42/hello", 3 },
    };
    for ( const auto& test_case : test_cases )
    {
        auto handler = r.route( GET, test_case.first );
        REQUIRE( static_cast< bool >( handler ) );
        REQUIRE( ( *handler )( ) == test_case.second );
    }

    std::vector< std::string > failure_tests = {
        "/abc/<int>/",
        "<int>",
        "/Abc/<int>/hello",
    };
    for ( const auto& test_case : failure_tests )
    {
        auto handler = r.route( GET, test_case );
        REQUIRE( !static_cast< bool >( handler ) );
    }
}

TEST_CASE( "You cannot reuse a parameter name in the same pattern" )
{
    using handler_t = std::function< void( ) >;
    webxx::router_t< handler_t > r{};

    REQUIRE_THROWS( r.push_back( "/<int:abc>/def/<int:abc>/", []( ) {} ) );
}

TEST_CASE( "A parameter_list, is used to hold an applied list of parameters" )
{
    webxx::path_parameter_list pl;

    SECTION( "We start empty" )
    {
        REQUIRE( pl.size( ) == 0 );
        REQUIRE( pl.begin( ) == pl.end( ) );
        REQUIRE_THROWS( pl.value( 0 ) );
        REQUIRE_THROWS( pl.name( 0 ) );
        REQUIRE_THROWS( pl.value( "not there" ) );
    }
    pl.push_back( "42" );

    SECTION( "We can add a parameter w/o a name and refence it" )
    {
        REQUIRE( pl.size( ) == 1 );
        REQUIRE( pl.begin( ) != pl.end( ) );
        REQUIRE( pl[ 0 ].first == "42" );
        REQUIRE( pl[ 0 ].second == "" );
        REQUIRE( pl.value( 0 ) == "42" );
        REQUIRE( pl.name( 0 ) == "" );
        REQUIRE( pl.begin( )->first == pl[ 0 ].first );
        REQUIRE( pl.begin( )->second == pl[ 0 ].second );
    }

    pl.push_back( "43", "almost_the_answer" );
    SECTION( "We can also add a parameter with a name" )
    {
        REQUIRE( pl.size( ) == 2 );
        REQUIRE( pl[ 1 ].first == "43" );
        REQUIRE( pl[ 1 ].second == "almost_the_answer" );
    }
    SECTION( "And reference it by position or name" )
    {
        REQUIRE( pl.value( 1 ) == "43" );
        REQUIRE( pl.name( 1 ) == "almost_the_answer" );
        REQUIRE( pl.value( "almost_the_answer" ) == "43" );
    }

    pl.push_back( "41", "almost_the_answer" );
    SECTION( "Adding a duplicate looks up fine by position, but not by name" )
    {
        REQUIRE( pl.size( ) == 3 );
        REQUIRE( pl.value( 2 ) == "41" );
        REQUIRE( pl.name( 2 ) == "almost_the_answer" );
        REQUIRE( pl.value( "almost_the_answer" ) == pl.value( 1 ) );
        REQUIRE( pl.value( "almost_the_answer" ) != pl.value( 2 ) );
    }

    pl.clear( );
    SECTION( "You can clear it as well" )
    {
        REQUIRE( pl.size( ) == 0 );
        REQUIRE( pl.begin( ) == pl.end( ) );
        REQUIRE_THROWS( pl.value( 0 ) );
    }

    SECTION( "You can build a parameter list from other items, including a "
             "initializer list" )
    {
        std::vector< webxx::path_parameter_list::value_type > input{
            { "5", "" },
            { "6", "3*2" },
        };
        webxx::path_parameter_list pl2{ input };
        REQUIRE( pl2.size( ) == 2 );
        REQUIRE( pl2.value( 1 ) == pl2.value( "3*2" ) );
        REQUIRE( pl2.value( 1 ) == "6" );
        REQUIRE( pl2.value( 0 ) == "5" );
    }
}

TEST_CASE( "You can do do routing and extract parameters" )
{
    using handler_t = std::function< int( ) >;

    webxx::router_t< handler_t > r{};

    auto GET = webxx::VERB::GET;
    auto POST = webxx::VERB::POST;

    r.push_back( "/abc/<int:val>/", []( ) -> int { return 1; } );
    r.push_back( "<int:thing>", []( ) -> int { return 2; } );
    r.push_back( "/Abc/<int:var>/hello", []( ) -> int { return 3; } );
    r.push_back( "/Abc/no_params/hello", []( ) -> int { return 4; } );
    r.push_back( "/Abc/<int:val1>/<int>/<int:val3>",
                 []( ) -> int { return 5; } );

    struct TestCaseWithParams
    {
        std::string                                           first;
        int                                                   second;
        std::vector< webxx::path_parameter_list::value_type > third;
    };

    std::vector< TestCaseWithParams > test_cases = {
        { "/abc/3/", 1, { { "3", "val" } } },
        { "/abc/0/", 1, { { "0", "val" } } },
        { "/abc/-1/", 1, { { "-1", "val" } } },
        { "-1", 2, { { "-1", "thing" } } },
        { "0", 2, { { "0", "thing" } } },
        { "4", 2, { { "4", "thing" } } },
        { "/Abc/-1/hello", 3, { { "-1", "var" } } },
        { "/Abc/0/hello", 3, { { "0", "var" } } },
        { "/Abc/42/hello", 3, { { "42", "var" } } },
        { "/Abc/no_params/hello", 4, {} },
        { "/Abc/5/4/6", 5, { { "5", "val1" }, { "4", "" }, { "6", "val3" } } },
    };
    for ( const auto& test_case : test_cases )
    {
        webxx::path_parameter_list ref_params( test_case.third );
        webxx::path_parameter_list returned_params{};
        auto handler = r.route( GET, test_case.first, &returned_params );
        REQUIRE( static_cast< bool >( handler ) );
        REQUIRE( ( *handler )( ) == test_case.second );
        REQUIRE( std::equal( ref_params.begin( ),
                             ref_params.end( ),
                             returned_params.begin( ),
                             returned_params.end( ) ) );
    }

    std::vector< std::string > failure_tests = {
        "/abc/<int>/",
        "<int>",
        "/Abc/<int>/hello",
    };
    for ( const auto& test_case : failure_tests )
    {
        auto handler = r.route( GET, test_case );
        REQUIRE( !static_cast< bool >( handler ) );
    }

    SECTION( "Test for failure w/ parameter extraction" )
    {
        webxx::path_parameter_list params;
        auto handler = r.route( GET, "/not a match", &params );
        REQUIRE( !static_cast< bool >( handler ) );
    }
}

TEST_CASE( "Routers can also note handlers for exceptional/special cases" )
{
    using handler_t = std::function< int( ) >;

    webxx::router_t< handler_t > r{};

    r.special( 500, []( ) { return 1; } );
    auto handler = r.special( 500 );
    REQUIRE( static_cast< bool >( handler ) );
    REQUIRE( ( *handler )( ) == 1 );
}
