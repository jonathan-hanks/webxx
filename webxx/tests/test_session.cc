//
// Created by jonathan.hanks on 2/22/20.
//
#include <webxx/middleware/session.hh>
#include <webxx/middleware/file_sessions.hh>

#include <catch.hpp>
#include <sys/stat.h>
#include <sys/types.h>

#include <boost/filesystem.hpp>

class TemporaryDirectory
{
public:
    TemporaryDirectory( )
    {
        auto root = boost::filesystem::temp_directory_path( );
        int  rc = 0;
        do
        {
            auto name = boost::filesystem::unique_path( );
            path_ = root / name;
            rc = ::mkdir( path_.string( ).c_str( ), 0700 );
        } while ( rc == EEXIST );
        if ( rc != 0 )
        {
            throw std::runtime_error( "Unable to create temporary directory" );
        }
    }
    TemporaryDirectory( const TemporaryDirectory& ) = delete;
    TemporaryDirectory( TemporaryDirectory&& ) = delete;
    TemporaryDirectory& operator=( const TemporaryDirectory& ) = delete;
    TemporaryDirectory& operator=( TemporaryDirectory&& ) = delete;
    ~TemporaryDirectory( )
    {
        try
        {
            boost::filesystem::remove_all( path_ );
        }
        catch ( ... )
        {
        }
    }
    const boost::filesystem::path&
    path( ) const noexcept
    {
        return path_;
    }

private:
    boost::filesystem::path path_;
};

TEST_CASE( "SessionManagers manage sessions" )
{
    TemporaryDirectory           tdir;
    webxx::middleware::SessionId id;

    webxx::middleware::FileSessionManager manager( tdir.path( ).string( ) );
    {
        webxx::middleware::Session s = manager.create_session( );
        s.data( ) = webxx::JsonType( 42 );
        manager.save( s );
        id = s.id( );
    }
    {
        auto s = manager.load( id );
        REQUIRE( (bool)s );
        REQUIRE( s->data( ).asInt( ) == 42 );
    }
}
