//
// Created by jonathan.hanks on 1/17/20.
//
#include "catch.hpp"
#include "webxx/simple_optional.hh"

#include <memory>
#include <type_traits>

using opt_int = webxx::simple_optional< int >;

TEST_CASE( "You can create a simple optional of an int" )
{
    webxx::simple_optional< int > opt;
    static_assert( std::is_same< int, opt_int::value_type >::value,
                   "The value_type must match the template parameter" );
    static_assert( sizeof( opt_int ) >= sizeof( opt_int::value_type ),
                   "The size of an optional needs to be at least the size of "
                   "the contained data" );
}

TEST_CASE( "You can create a empty simple_optional with webxx::none" )
{
    webxx::simple_optional< int > opt{ webxx::none };
    REQUIRE( !( (bool)opt ) );
    opt = 42;
    REQUIRE( (bool)opt );
}

TEST_CASE( "You can assign webxx::none to empty a simple_optional" )
{
    webxx::simple_optional< int > opt{ 42 };
    REQUIRE( (bool)opt );
    opt = webxx::none;
    REQUIRE( !( (bool)opt ) );
}

TEST_CASE( "You can move construct a simple_optional" )
{
    webxx::simple_optional< int > other( 42 );
    REQUIRE( (bool)other );
    webxx::simple_optional< int > opt( std::move( other ) );
    REQUIRE( !( (bool)other ) );
    REQUIRE( (bool)opt );
}

TEST_CASE( "You can assign a value into a simple optional and read it out" )
{
    opt_int opt;
    opt = 5;
    REQUIRE( (bool)opt );

    auto tmp = *opt;
    REQUIRE( tmp == 5 );

    REQUIRE( (bool)opt );
}

TEST_CASE( "You can test to see when a optional contains a value" )
{
    opt_int opt;

    REQUIRE( !(bool)opt );
    opt = 5;
    REQUIRE( (bool)opt );
}

TEST_CASE(
    "You can assign a value into a simple optional at construction time" )
{
    opt_int opt( 5 );
    REQUIRE( (bool)opt );
    REQUIRE( *opt == 5 );
}

TEST_CASE( "You can assign a value into a simple optional at construction time "
           "with a move" )
{
    auto shared = std::make_shared< int >( 42 );
    int* p = shared.get( );
    using opt_shared_int = webxx::simple_optional< std::shared_ptr< int > >;
    opt_shared_int opt( std::move( shared ) );
    REQUIRE( (bool)opt );
    REQUIRE( !(bool)shared );
    shared = *opt;
    REQUIRE( (bool)shared );
    REQUIRE( p == shared.get( ) );
}

TEST_CASE( "You can clear a optional by calling clear" )
{
    opt_int opt;
    REQUIRE( !(bool)opt );
    opt = 5;
    REQUIRE( (bool)opt );
    opt.clear( );
    REQUIRE( !(bool)opt );
}

TEST_CASE( "clear calls the destructor if needed" )
{
    static int destructions = 0;
    class test_class
    {
    public:
        ~test_class( )
        {
            destructions++;
        }
    };
    webxx::simple_optional< test_class > opt;
    REQUIRE( !(bool)opt );
    opt = test_class( );
    REQUIRE( (bool)opt );
    opt.clear( );
    REQUIRE( !(bool)opt );
    REQUIRE( destructions == 2 );
}

TEST_CASE( "clear calls the destructor if needed when assigned by webxx::none" )
{
    static int destructions = 0;
    class test_class
    {
    public:
        ~test_class( )
        {
            destructions++;
        }
    };
    webxx::simple_optional< test_class > opt;
    REQUIRE( !(bool)opt );
    opt = test_class( );
    REQUIRE( (bool)opt );
    opt = webxx::none;
    REQUIRE( !(bool)opt );
    REQUIRE( destructions == 2 );
}

TEST_CASE( "Simple optionals call the destructor of their value_type if they "
           "contain a value" )
{
    static int destructions = 0;
    class test_class
    {
    public:
        ~test_class( )
        {
            destructions++;
        }
    };
    {
        webxx::simple_optional< test_class > opt;
    }
    REQUIRE( destructions == 0 );
    {
        webxx::simple_optional< test_class > opt( ( test_class( ) ) );
    }
    REQUIRE( destructions == 2 );
    destructions = 0;
    {
        webxx::simple_optional< test_class > opt( ( test_class( ) ) );
        test_class                           t;
        opt = t;
    }
    REQUIRE( destructions == 4 );
}

TEST_CASE( "You can move assign T into an optional" )
{
    auto tmp = std::make_shared< int >( 42 );
    using opt_shared_int = webxx::simple_optional< std::shared_ptr< int > >;

    opt_shared_int opt;
    REQUIRE( !(bool)opt );
    opt = std::move( tmp );
    REQUIRE( (bool)opt );
    REQUIRE( !(bool)tmp );
}

TEST_CASE( "Moving out of an optional is explicit" )
{
    std::unique_ptr< int >                           p( new int( 5 ) );
    webxx::simple_optional< std::unique_ptr< int > > opt;
    opt = std::move( p );
    REQUIRE( (bool)opt );
    REQUIRE( !(bool)p );
    p = opt.move_out( );
    REQUIRE( !(bool)opt );
    REQUIRE( (bool)p );
}

TEST_CASE( "You can assign to an optional with another compatible optional" )
{
    opt_int o1;
    opt_int o2( 5 );

    o1 = o2;
    REQUIRE( (bool)o1 );
    REQUIRE( (bool)o2 );
    REQUIRE( *o1 == 5 );
}

TEST_CASE(
    "You can move assign to an optional with another compatible optional" )
{
    opt_int o1;
    opt_int o2( 5 );

    o1 = std::move( o2 );
    REQUIRE( (bool)o1 );
    REQUIRE( !(bool)o2 );
    REQUIRE( *o1 == 5 );
}

TEST_CASE( "You can dereference into an optional with ->" )
{
    struct stuff
    {
        int a{ 1 };
        int b{ 2 };
    };

    webxx::simple_optional< stuff > opt( stuff{} );
    REQUIRE( (bool)opt );
    REQUIRE( opt->a == 1 );
}

TEST_CASE( "You can safely dereference into an optional with at" )
{
    struct stuff
    {
        int a{ 1 };
        int b{ 2 };
    };

    webxx::simple_optional< stuff > opt;
    REQUIRE( !( (bool)opt ) );
    REQUIRE_THROWS_AS( opt.at( ), webxx::bad_optional );
    opt = stuff{};
    REQUIRE( opt.at( ).a == 1 );
}