//
// Created by jonathan.hanks on 10/18/19.
//

#include <webxx/detail/chunked_encoding_stream.hh>
#include <catch.hpp>

#include <array>
#include <ostream>
#include <vector>

namespace
{
    struct noop_io
    {
        template < typename ConstBufSeq >
        boost::system::error_code
        operator( )( ConstBufSeq&& bufs )
        {
            return boost::system::error_code{};
        }
    };

    struct string_io
    {
        template < typename ConstBufSeq >
        boost::system::error_code
        operator( )( ConstBufSeq&& bufs )
        {
            std::vector< char > tmp;
            tmp.resize( boost::asio::buffer_size( bufs ) );
            boost::asio::buffer_copy( boost::asio::buffer( tmp ), bufs );

            str.append( tmp.data( ), tmp.size( ) );

            return boost::system::error_code{};
        }

        std::string str{};
    };
} // namespace

TEST_CASE( "You can create a fixed buffer stream buf to do chunked encoding" )
{
    noop_io                                               io;
    std::array< char, 100 >                               backing_store{};
    webxx::detail::chunked_encoding_stream_buf< noop_io > bstream(
        io, boost::asio::buffer( backing_store ) );
    REQUIRE( !bstream.closed( ) );
}

TEST_CASE( "You can create a fixed buffer stream buf to do chunked encoding, "
           "it must obey size constraints" )
{
    noop_io                 io;
    std::array< char, 100 > backing_store{};
    REQUIRE_THROWS( webxx::detail::chunked_encoding_stream_buf< noop_io >(
        io, boost::asio::buffer( backing_store.data( ), 15 ) ) );
    REQUIRE_THROWS( webxx::detail::chunked_encoding_stream_buf< noop_io >(
        io, boost::asio::buffer( backing_store.data( ), 10000000000 ) ) );
    {
        webxx::detail::chunked_encoding_stream_buf< noop_io > bstream(
            io, boost::asio::buffer( backing_store.data( ), 16 ) );
    }
}

TEST_CASE(
    "A fixed buffer stream buf closes itself down by sending an empty chunk" )
{
    string_io               io;
    std::array< char, 100 > backing_store{};

    {
        webxx::detail::chunked_encoding_stream_buf< string_io > bstream(
            io, boost::asio::buffer( backing_store ) );
    }
    REQUIRE( io.str == "0\r\n\r\n" );
}

TEST_CASE( "A fixed buffer stream buf can be explicitly closed down and only "
           "sends the empty chunk once" )
{
    string_io               io;
    std::array< char, 100 > backing_store{};

    {
        REQUIRE( io.str.empty( ) );
        webxx::detail::chunked_encoding_stream_buf< string_io > bstream(
            io, boost::asio::buffer( backing_store ) );
        REQUIRE( !bstream.closed( ) );
        bstream.close_stream( );
        REQUIRE( io.str == "0\r\n\r\n" );
        REQUIRE( bstream.closed( ) );
        io.str = "";
    }
    REQUIRE( io.str.empty( ) );
}

TEST_CASE( "You can used the fixed stream buffer with an iostream" )
{
    string_io              io;
    std::array< char, 25 > backing_store{};
    {
        webxx::detail::chunked_encoding_stream_buf< string_io > bstream(
            io, boost::asio::buffer( backing_store ) );
        std::ostream os( &bstream );

        REQUIRE( io.str.empty( ) );
        REQUIRE( !bstream.closed( ) );
        REQUIRE( bstream.remaining_buffer( ) == 25 - 12 - 2 - 1 );
        for ( char i = 0; i < 11; ++i )
        {
            char ch = static_cast< char >( 'a' ) + i;
            os << ch;
        }
        REQUIRE( io.str == "B\r\nabcdefghijk\r\n" );
        REQUIRE( bstream.remaining_buffer( ) == 25 - 12 - 2 - 1 );
        io.str = "";
        os << "hello";
        os.flush( );
        REQUIRE( io.str == "5\r\nhello\r\n" );
        io.str = "";
        os << "world!";
        REQUIRE( io.str.empty( ) );
    }
    REQUIRE( io.str == "6\r\nworld!\r\n0\r\n\r\n" );
}

TEST_CASE( "You can used the fixed stream buffer with an iostream, closing the "
           "stream causes an failed io stream if you still use it" )
{
    string_io              io;
    std::array< char, 25 > backing_store{};
    {
        webxx::detail::chunked_encoding_stream_buf< string_io > bstream(
            io, boost::asio::buffer( backing_store ) );
        std::ostream os( &bstream );

        REQUIRE( io.str.empty( ) );
        REQUIRE( !bstream.closed( ) );
        REQUIRE( bstream.remaining_buffer( ) == 25 - 12 - 2 - 1 );
        for ( char i = 0; i < 11; ++i )
        {
            char ch = static_cast< char >( 'a' ) + i;
            os << ch;
        }
        REQUIRE( io.str == "B\r\nabcdefghijk\r\n" );
        REQUIRE( bstream.remaining_buffer( ) == 25 - 12 - 2 - 1 );
        io.str = "";
        os << "hello";
        os.flush( );
        REQUIRE( io.str == "5\r\nhello\r\n" );
        io.str = "";
        os << "world!";
        REQUIRE( io.str.empty( ) );
        bstream.close_stream( );

        REQUIRE( io.str == "6\r\nworld!\r\n0\r\n\r\n" );
        REQUIRE( bstream.closed( ) );
        REQUIRE( os.good( ) );
        io.str = "";
        os << 1;
        os.flush( );
        REQUIRE( os.fail( ) );
        REQUIRE( io.str.empty( ) );
        REQUIRE( bstream.closed( ) );
    }
    REQUIRE( io.str.empty( ) );
}