//
// Created by jonathan.hanks on 2/28/20.
//
#include <webxx/detail/multipart.hh>

#include "catch.hpp"

std::string sample_boundary =
    "---------------------------14016255431922936949929284329";
std::string sample_data =
    "-----------------------------14016255431922936949929284329\r\n"
    "Content-Disposition: form-data; name=\"csrf_token\"\r\n"
    "\r\n"
    "a9d65889a03203645f4c5d02451ab65d72f78af4e50a1919\r\n"
    "-----------------------------14016255431922936949929284329\r\n"
    "Content-Disposition: form-data; name=\"thing1\"\r\n"
    "\r\n"
    "a\r\n"
    "-----------------------------14016255431922936949929284329\r\n"
    "Content-Disposition: form-data; name=\"thing2\"\r\n"
    "\r\n"
    "b\r\n"
    "-----------------------------14016255431922936949929284329--\r\n";

std::string sample_file_data =
    "-----------------------------14016255431922936949929284329\r\nContent-"
    "Disposition: form-data; "
    "name=\"csrf_"
    "token\"\r\n\r\n76bc2256e56413a05b710db1dcc9a0bddc93eff935f1ee3b\r\n-------"
    "----------------------14016255431922936949929284329\r\nContent-"
    "Disposition: "
    "form-data; "
    "name=\"thing1\"\r\n\r\nabc\r\n-----------------------------"
    "14016255431922936949929284329\r\nContent-Disposition: form-data; "
    "name=\"thing2\"; filename=\"hello.c\"\r\nContent-Type: "
    "text/plain\r\n\r\n#include <stdio.h>\n\nint main() {\n    printf(\"Hello "
    "World!\\n\");\n}\r\n-----------------------------"
    "14016255431922936949929284329--\r\n";

TEST_CASE( "You can parse multipart data" )
{
    auto params =
        webxx::detail::decode_multipart( sample_data, sample_boundary );
    REQUIRE( (bool)params );
    REQUIRE( params.at( )[ "csrf_token" ] ==
             "a9d65889a03203645f4c5d02451ab65d72f78af4e50a1919" );
    REQUIRE( params.at( )[ "thing1" ] == "a" );
    REQUIRE( params.at( )[ "thing2" ] == "b" );
}

TEST_CASE( "You can extract a boundary string from a content type field" )
{
    std::string input =
        "multipart/form-data; "
        "boundary=---------------------------14016255431922936949929284329";
    std::string output = webxx::detail::extract_boundary_value( input );
    REQUIRE( output ==
             "---------------------------14016255431922936949929284329" );
}

TEST_CASE( "You can parse multipart data with files in it" )
{
    auto params =
        webxx::detail::decode_multipart( sample_data, sample_boundary );
    REQUIRE( (bool)params );
}