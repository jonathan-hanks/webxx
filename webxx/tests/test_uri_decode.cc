//
// Created by jonathan.hanks on 8/27/19.
//
#include "webxx/uri.hh"

#include <catch.hpp>

TEST_CASE( "You can decode a uri_t" )
{
    std::vector< std::pair< std::string, webxx::uri_t > > test_cases{
        { "/abc/", { "/abc/", {} } },
        { "/a%20c/", { "/a c/", {} } },
        { "/a%20c/def/?", { "/a c/def/", {} } },
        { "/", { "/", {} } },
        { "", { "", {} } },
        { "/?a=b", { "/", { { "a", "b" } } } },
        { "/?a+b=c+d", { "/", { { "a b", "c d" } } } },
        { "/?a%2=c+d", { "/", {} } },
        { "/?a=c+d%", { "/", {} } },
        { "/?a=c&b=d", { "/", { { "a", "c" }, { "b", "d" } } } },
        { "/?a=c&&b=d", { "/", { { "a", "c" }, { "b", "d" } } } },
        { "/?a=c&b=d&", { "/", { { "a", "c" }, { "b", "d" } } } },
        { "/?a=c&b=d&e", { "/", { { "a", "c" }, { "b", "d" }, { "e", "" } } } },
    };
    for ( const auto& test_case : test_cases )
    {
        webxx::simple_optional< webxx::uri_t > uri =
            webxx::parse_uri( test_case.first );

        REQUIRE( static_cast< bool >( uri ) );
        REQUIRE( uri->path == test_case.second.path );
        REQUIRE( uri->params == test_case.second.params );
    }
}

TEST_CASE( "You can decode url-encoded parameters" )
{
    std::vector< std::pair< std::string, webxx::query_params_t > > test_cases{
        { "a=b", { { "a", "b" } } },
        { "a+b=c+d", { { "a b", "c d" } } },
        { "a%2=c+d", {} },
        { "a=c+d%", {} },
        { "a=c&b=d", { { "a", "c" }, { "b", "d" } } },
        { "a=c&&b=d", { { "a", "c" }, { "b", "d" } } },
        { "a=c&b=d&", { { "a", "c" }, { "b", "d" } } },
        { "a=c&b=d&e", { { "a", "c" }, { "b", "d" }, { "e", "" } } },
    };
    for ( const auto& test_case : test_cases )
    {
        webxx::simple_optional< webxx::query_params_t > params =
            webxx::url_decode_params( test_case.first );

        REQUIRE( static_cast< bool >( params ) );
        REQUIRE( *params == test_case.second );
    }
}

TEST_CASE( "You can urldecode strings" )
{
    std::vector< std::pair< std::string, std::string > > test_cases = {
        { "abc", "abc" },   { "a+c", "a c" },   { "a%20c", "a c" },
        { "ab%20", "ab " }, { "%20bc", " bc" }, { "%20", " " },
        { "%2f", "/" },     { "a%2fb", "a/b" }, { "a%2Fb", "a/b" },
        { "a%2bb", "a+b" }, { "a%2Bb", "a+b" },
    };

    for ( const auto& test_case : test_cases )
    {
        webxx::simple_optional< std::string > result =
            webxx::urldecode( test_case.first );
        REQUIRE( static_cast< bool >( result ) );
        REQUIRE( *result == test_case.second );
    }

    std::vector< std::string > failure_cases = {
        "%", "%g5", "%G5", "%5Z", "%5z", "%0", "abcd%0", "abcd%0G", "abcd%",
    };
    for ( const auto& test_case : failure_cases )
    {
        webxx::simple_optional< std::string > result =
            webxx::urldecode( test_case );
        REQUIRE( !static_cast< bool >( result ) );
    }
}