//
// Created by jonathan.hanks on 2/11/20.
//
#include <webxx/middleware/cookie.hh>

#include <catch.hpp>

#include <algorithm>
#include <iterator>
#include <map>
#include <unordered_set>
#include <string>

#include <webxx/req_resp.hh>

namespace
{
    webxx::request_t
    make_req( std::string path, std::map< std::string, std::string > headers )
    {
        webxx::uri_t                  uri{ { std::move( path ) }, {} };
        webxx::request_t::HeadersType req_headers;
        for ( const auto& entry : headers )
        {
            req_headers.insert( entry.first, entry.second );
        }
        return webxx::request_t( webxx::VERB::GET, uri, {}, req_headers );
    }
} // namespace

TEST_CASE( "You can extract cookies from a request" )
{
    auto req1 = make_req( "/", {} );
    auto req2 = make_req( "/", { { "Cookie", "alpha=abc; hungry=food" } } );

    {
        webxx::middleware::cookie_jar jar =
            webxx::middleware::extract_cookies( req1 );
        REQUIRE( jar.empty( ) );
    }
    {
        webxx::middleware::cookie_jar jar =
            webxx::middleware::extract_cookies( req2 );
        REQUIRE( jar.size( ) == 2 );
        REQUIRE( jar.at( "alpha" ) == "abc" );
        REQUIRE( jar.at( "hungry" ) == "food" );
    }
}

TEST_CASE( "The cookie middleware extracts cookies into a request and places "
           "them in a response" )
{
    auto req = make_req( "/", { { "Cookie", "alpha=abc; hungry=food" } } );
    auto handler = webxx::middleware::cookie_middleware(
        []( webxx::request_t& request ) -> webxx::response_ptr {
            auto jar = boost::any_cast< webxx::middleware::cookie_jar* >(
                request.app_data( ).at( "cookies" ) );
            jar->insert( std::make_pair( "frog", "green" ) );
            return webxx::string_response( 200, "Hi there" );
        } );
    auto resp = handler( req );
    auto header_range = resp->headers( ).equal_range( "Set-Cookie" );
    std::unordered_set< std::string > expected = {
        "alpha=abc",
        "hungry=food",
        "frog=green",
    };
    std::unordered_set< std::string > actual{};
    std::transform(
        header_range.first,
        header_range.second,
        std::inserter( actual, actual.end( ) ),
        []( const webxx::request_t::HeadersType::value_type& cur )
            -> std::string { return std::string( cur.value( ) ); } );
    REQUIRE( expected == actual );
}
