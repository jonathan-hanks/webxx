/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <webxx/middleware/csrf.hh>
#include <webxx/middleware/session.hh>
#include <webxx/detail/hex.hh>
#include <boost/any.hpp>

namespace webxx
{
    namespace middleware
    {
        std::string
        csrf_token( webxx::request_t& req )
        {
            auto session_it = req.app_data( ).find( "session" );
            if ( session_it == req.app_data( ).end( ) )
            {
                return "";
            }
            webxx::middleware::Session* session =
                boost::any_cast< webxx::middleware::Session* >(
                    req.app_data( ).at( "session" ) );
            if ( !session )
            {
                return "";
            }
            auto csrf_token = session->data( ).get( "csrf", 0 );
            if ( !csrf_token.isString( ) )
            {
                return "";
            }
            return webxx::detail::to_hex_str( csrf_token.asString( ) );
        }
    } // namespace middleware
} // namespace webxx