/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) 2020 Jonathan Hanks

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <webxx/middleware/cookie.hh>

#include <boost/algorithm/string/find_iterator.hpp>
#include <boost/algorithm/string/finder.hpp>

namespace webxx
{
    namespace middleware
    {

        cookie_jar
        extract_cookies( const request_t& req )
        {
            cookie_jar cookies{};
            auto       hdr_it = req.headers( ).find( "Cookie" );
            if ( hdr_it != req.headers( ).end( ) )
            {
                std::string cookie_str =
                    std::string( req.headers( )[ "Cookie" ] );
                std::string delim( "; " );
                auto        split_it = boost::make_split_iterator(
                    cookie_str,
                    boost::first_finder( delim, boost::is_equal( ) ) );
                auto range = boost::make_iterator_range(
                    split_it, decltype( split_it )( ) );
                for ( const auto& segment : range )
                {
                    auto it =
                        std::find( segment.begin( ), segment.end( ), '=' );
                    if ( it == segment.end( ) )
                    {
                        continue;
                    }
                    cookies.insert( std::make_pair(
                        std::string( segment.begin( ), it ),
                        std::string( it + 1, segment.end( ) ) ) );
                }
            }
            return cookies;
        }
    } // namespace middleware
} // namespace webxx
