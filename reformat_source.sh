#!/bin/bash

if which clang-format-7 > /dev/null; then
    CFORMAT=`which clang-format-7`
elif which clang-format > /dev/null; then
    CFORMAT=`which clang-format`
else
    echo "Could not find clang format"
    exit 1
fi

SOURCE_DIRS="webxx examples"

for dir in ${SOURCE_DIRS}; do
    echo "Reformatting in ${dir}"
    find ${dir} -type f -iregex ".*\.\(c\|cc\|cxx\|cpp\|h\|hh\|hpp\|hxx\)" \! -iname catch.hpp \! -iname sqlite3.\* \! -iname bash_pattern.\* -exec "${CFORMAT}" -i -style=file -fallback-style=none {} \;
done
