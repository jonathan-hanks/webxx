/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <webxx/webxx.hh>
#include <webxx/middleware/csrf.hh>
#include <webxx/middleware/cookie.hh>
#include <webxx/middleware/file_sessions.hh>
#include <webxx/middleware/session.hh>

#include <iostream>

#include "templates/index.kiste.hh"
#include "templates/form_page.kiste.hh"

#include <kiste/html.h>

template < typename Callable, typename Data >
webxx::response_ptr
kiste_html( const Data& data, Callable&& generator )
{
    std::ostringstream os;
    auto               serializer = kiste::html{ os };
    auto               template_render = generator( data, serializer );
    template_render.render( );
    return webxx::string_response( 200, os.str( ) );
}

webxx::response_ptr
index_handler( webxx::request_t& req )
{
    struct Data
    {
    } data;
    return kiste_html( data, http_templ::Index );
}

webxx::response_ptr
form_handler( webxx::request_t& req )
{
    struct Data
    {
        webxx::request_t& req;
    };
    auto resp = kiste_html( Data{ req }, http_templ::FormPage );
    resp->headers( ).insert( "Set-Cookie", "frog=green" );
    resp->headers( ).insert( "Set-Cookie", "ball=blue" );
    resp->headers( ).insert( boost::beast::http::field::set_cookie,
                             "block=red" );

    for ( const auto& hdr : resp->headers( ) )
    {
        std::cout << hdr.name_string( ) << ": " << std::string( hdr.value( ) )
                  << "\n";
    }
    if ( req.post_params( ) )
    {
        if ( true )
        {

            webxx::middleware::Session* session =
                boost::any_cast< webxx::middleware::Session* >(
                    req.app_data( )[ "session" ] );
            webxx::JsonType data{};
            std::string     str_a = req.post_params( ).at( ).at( "thing1" );
            data[ "thing1" ] = str_a;
            data[ "thing2" ] = req.post_params( ).at( ).at( "thing2" );
            session->data( ) = data;
        }
    }
    return resp;
}

webxx::legacy_evaluation
evaluate_legacy_input(boost::asio::const_buffer input)
{
    if (input.size() < 5)
    {
        return webxx::legacy_evaluation::maybe_legacy;
    }
    std::string debug((char*)input.data(), input.size());
    std::cout << "legacy input = " << debug << std::endl;
    return (strncmp(reinterpret_cast<const char*>(input.data()), "hello", 5) == 0 ?
        webxx::legacy_evaluation::legacy
                 : webxx::legacy_evaluation::not_legacy
    );
}

void
handle_legacy_input(boost::beast::flat_buffer& input, webxx::LegacyIO& io)
{
    input.consume(input.size());
    io.write(boost::asio::buffer("world!\n"));
}



int
main( int argc, char* argv[] )
{
    webxx::middleware::FileSessionManager session_man( "/tmp/webxx_sessions" );
    webxx::Server                         server( {}, 3 );
    server.router( ).push_back( "/", index_handler );
    server.router( ).push_back(
        "/form",
        webxx::middleware::cookie_middleware(
            webxx::middleware::session_middleware(
                session_man,
                webxx::middleware::csrf_middleware( form_handler ) ) ) );
    server.router().setup_legacy(
                        evaluate_legacy_input,
                        handle_legacy_input);

    server.run( );
    return 0;
}