/*<one line to give the program's name and a brief idea of what it does.>
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <functional>
#include <iostream>
#include <string>
#include <thread>

#include <boost/asio.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/error.hpp>

#include "webxx/webxx.hh"

webxx::response_ptr
index_handler( webxx::request_t& req )
{
    std::cout << "Answering request in thread" << std::this_thread::get_id( )
              << std::endl;

    auto resp = webxx::string_response(
        200, "<html><body><p>The index!</p></body></html>" );
    return resp;
}

webxx::response_ptr
not_found( webxx::request_t& request )
{

    std::cout << "Not found request in thread" << std::this_thread::get_id( )
              << std::endl;
    return webxx::string_response(
        404, "<html><body><p>Not Found!</p></body></html>" );
}

webxx::response_ptr
stream( webxx::request_t& request )
{

    std::cout << "Stream request in thread" << std::this_thread::get_id( )
              << std::endl;
    return webxx::continuation_response( 200, []( std::ostream& os ) {
        os << "<html>\n"
              "<body>\n";
        for ( int i = 0; i < 1000000; ++i )
        {
            os << " " << i;
            if ( i && ( i % 10 == 0 ) )
            {
                os << "</p>\n";
            }
        }
        os << "</body>\n"
              "</html>\n";
    } );
}

int
main( )
{
    std::cout << "Hello, World!" << std::endl;
    webxx::Server server( webxx::Endpoint{ "127.0.0.1" }, 3 );
    server.router( ).push_back( "/", index_handler );
    server.router( ).push_back( "/favicon.ico", not_found );
    server.router( ).push_back( "/stream", stream );
    server.run( );
    return 0;
}
